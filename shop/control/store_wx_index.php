<?php
/**
 * 商户自定义微官网首页内容
 *
 *
 *
*/


defined('BYshopJL') or exit ('Access Invalid!');

class store_wx_indexControl extends BaseSellerControl
{
    public function __construct()
    {
        parent::__construct();
        Language::read('member_store_goods_index');
    }

    public function indexOp()
    {
        $this->goods_listOp();
    }

    public function add_goods_indexOp()
    {
        $model_goods = Model('goods');
        $model_wx_index = Model('store_wx_index');

        $result = $model_wx_index->getby_store_id($_SESSION['store_id']);
        if($result){
            $goods_id_array = unserialize($result['goods_id_list']);
        }
        $map['store_id'] = $_SESSION['store_id'];
        $goods_id_array = $model_wx_index->getStoreIndexGoodsId($map);
        $where = array();
        $where['store_id'] = $_SESSION['store_id'];
        $where['goods_id'] = array('not in', $goods_id_array);
        if (intval($_GET['stc_id']) > 0) {
            $where['goods_stcids'] = array('like', '%,' . intval($_GET['stc_id']) . ',%');
        }
        if (trim($_GET['keyword']) != '') {
            switch ($_GET['search_type']) {
                case 0:
                    $where['goods_name'] = array('like', '%' . trim($_GET['keyword']) . '%');
                    break;
                case 1:
                    $where['goods_serial'] = array('like', '%' . trim($_GET['keyword']) . '%');
                    break;
                case 2:
                    $where['goods_commonid'] = intval($_GET['keyword']);
                    break;
            }
        }
        $goods_list = $model_goods->getGoodsOnlineList($where, '*', 10);
        Tpl::output('goods_id_array', $goods_id_array);
        Tpl::output('show_page', $model_goods->showpage());
        Tpl::output('goods_list', $goods_list);

        // 商品分类
        $store_goods_class = Model('store_goods_class')->getClassTree(array('store_id' => $_SESSION['store_id'], 'stc_state' => '1'));
        Tpl::output('store_goods_class', $store_goods_class);

        $this->profile_menu('goods_list', 'goods_list');
        Tpl::showpage('store_wx_index.add');
    }

    /**
     * 出售中的商品列表
     */
    public function goods_listOp()
    {
        $model_goods = Model('goods');
        $model_wx_index = Model('store_wx_index');
        $map['store_id'] = $_SESSION['store_id'];
        $goods_id_array = $model_wx_index->getStoreIndexGoodsId($map);
        $where = array();
        $where['store_id'] = $_SESSION['store_id'];
        $where['goods_id'] = array('in', $goods_id_array);
        $goods_list = $model_goods->getGoodsOnlineList($where, '*', 10);

        Tpl::output('show_page', $model_goods->showpage());
        Tpl::output('goods_list', $goods_list);

        $this->profile_menu('goods_list', 'goods_list');
        Tpl::showpage('store_wx_index');
    }


    public function add_goodsOp()
    {
        $model_wx_index = Model('store_wx_index');
        $goods_id = $_GET['goods_id'];
        if (!$goods_id) showDialog('参数错误', '', 'error');

        $goods_array = explode(',', $goods_id);
        //判断是否存在首页配置
        $result = $model_wx_index->getby_store_id($_SESSION['store_id']);
        if ($result) {
            $map['store_id'] = $_SESSION['store_id'];
            $goods_id_array = unserialize($result['goods_id_list']);
            $data = array(
                'goods_id_list' => serialize(array_flip(array_flip(array_merge($goods_id_array, $goods_array))))
            );
            $rs = $model_wx_index->updateIndex($map, $data);

        } else {
            $data = array(
                'store_id' => $_SESSION['store_id'],
                'goods_id_list' => serialize($goods_array)
            );
            $rs = $model_wx_index->addIndex($data);
        }

        if ($rs) {
            showDialog('添加成功', 'reload', 'succ');
        } else {
            showDialog('添加失败', '', 'error');
        }

    }

    /**
     * 删除商品
     */
    public function drop_goodsOp()
    {
        $model_wx_index = Model('store_wx_index');
        $map['store_id'] = $_SESSION['store_id'];
        $goods_id_array = $model_wx_index->getStoreIndexGoodsId($map);


        $goods_id = $_GET['goods_id'];
        if (!$goods_id) showDialog('参数错误', '', 'error');

        $goods_array = explode(',', $goods_id);
        foreach ($goods_array as $v) {
            $key = array_search($v, $goods_id_array);

            if ($key !== false) unset($goods_id_array[$key]);

        }
        $data = array(
            'goods_id_list' => serialize($goods_id_array)
        );
        $rs = $model_wx_index->updateIndex($map, $data);
        if ($rs) {
            showDialog(L('store_goods_index_goods_del_success'), 'reload', 'succ');
        } else {
            showDialog(L('store_goods_index_goods_del_fail'), '', 'error');
        }
    }


    /**
     * 用户中心右边，小导航
     *
     * @param string $menu_type 导航类型
     * @param string $menu_key 当前导航的menu_key
     * @param boolean $allow_promotion
     * @return
     */
    private function profile_menu($menu_type, $menu_key, $allow_promotion = array())
    {
        $menu_array = array();
        switch ($menu_type) {
            case 'goods_list':
                $menu_array = array(
                    array('menu_key' => 'goods_list', 'menu_name' => '微网站首页产品', 'menu_url' => urlShop('store_wx_index', 'index'))
                );
                break;
            case 'edit_detail':
                if ($allow_promotion['lock'] === false) {
                    $menu_array = array(
                        array('menu_key' => 'edit_detail', 'menu_name' => '编辑商品', 'menu_url' => urlShop('store_goods_online', 'edit_goods', array('commonid' => $_GET['commonid'], 'ref_url' => $_GET['ref_url']))),
                        array('menu_key' => 'edit_image', 'menu_name' => '编辑图片', 'menu_url' => urlShop('store_goods_online', 'edit_image', array('commonid' => $_GET['commonid'], 'ref_url' => ($_GET['ref_url'] ? $_GET['ref_url'] : getReferer())))),
                    );
                }
                if ($allow_promotion['gift']) {
                    $menu_array[] = array('menu_key' => 'add_gift', 'menu_name' => '赠送赠品', 'menu_url' => urlShop('store_goods_online', 'add_gift', array('commonid' => $_GET['commonid'], 'ref_url' => ($_GET['ref_url'] ? $_GET['ref_url'] : getReferer()))));
                }
                if ($allow_promotion['combo']) {
                    $menu_array[] = array('menu_key' => 'add_combo', 'menu_name' => '推荐组合', 'menu_url' => urlShop('store_goods_online', 'add_combo', array('commonid' => $_GET['commonid'], 'ref_url' => ($_GET['ref_url'] ? $_GET['ref_url'] : getReferer()))));
                }
                break;
            case 'edit_class':
                $menu_array = array(
                    array('menu_key' => 'edit_class', 'menu_name' => '选择分类', 'menu_url' => urlShop('store_goods_online', 'edit_class', array('commonid' => $_GET['commonid'], 'ref_url' => $_GET['ref_url']))),
                    array('menu_key' => 'edit_detail', 'menu_name' => '编辑商品', 'menu_url' => urlShop('store_goods_online', 'edit_goods', array('commonid' => $_GET['commonid'], 'ref_url' => $_GET['ref_url']))),
                    array('menu_key' => 'edit_image', 'menu_name' => '编辑图片', 'menu_url' => urlShop('store_goods_online', 'edit_image', array('commonid' => $_GET['commonid'], 'ref_url' => ($_GET['ref_url'] ? $_GET['ref_url'] : getReferer())))),
                );
                break;
        }
        Tpl::output('member_menu', $menu_array);
        Tpl::output('menu_key', $menu_key);
    }

}
