$(function() {
    if(!Store_id){
        alert('参数错误');
        wx_redirect(get_root_url()+'/wx');
    }
    $.ajax({
        url: ApiUrl + "/index.php?act=store_index&store_id="+Store_id,
        type: 'get',
        dataType: 'json',
        success: function(result) {
            var data = result.datas;
            var html = '';
            if(data.error){
                alert(data.error);
            }
            html += template.render('goods', data);
            $("#main-container").html(html);

            bind4ChangHref();
            $('title').html(data.store_info.store_name);
        }
    });
});
