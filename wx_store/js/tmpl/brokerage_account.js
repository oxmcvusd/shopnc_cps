$(function(){
    var key = getcookie('key');

    /*填充表单*/
    $.ajax({
        type:'get',
        url:ApiUrl+'/index.php?act=member_brokerage&op=brokerage_account',
        dataType:'json',
        success:function(result){
            if(result.code==1){
                $('.wx_bank_no').val(result.datas.bk_bank_no);
                $('.wx_bank_user').val(result.datas.bk_bank_user);
                $('.wx_id_number').val(result.datas.bk_id_number);
                return false;
            }
        }
    });


    //提交操作
    $('.wx_user_mobile_confirm').click(function(){
        var bank_no = $('.wx_bank_no').val();
        var bank_user = $('.wx_bank_user').val();
        var id_number = $('.wx_id_number').val();
        if(!bank_no){
            $('.error-tips').html('支付宝账号不可为空');
            $('.error-tips').show();
            return false;
        }
        if(!bank_user){
            $('.error-tips').html('真实姓名不可为空');
            $('.error-tips').show();
            return false;
        }
        if(!id_number){
            $('.error-tips').html('身份证号不可为空');
            $('.error-tips').show();
            return false;
        }
        //绑定手机
        $.ajax({
            type:'post',
            url:ApiUrl+'/index.php?act=member_brokerage&op=brokerage_account',
            data:{'bank_no':bank_no,'bank_user':bank_user,'id_number':id_number,'key':key},
            dataType:'json',
            success:function(result){
                if(result.code==1){
                    $.sDialog({
                        skin:"block",
                        content:result.msg,
                        okBtnText: "去用户中心",
                        cancelBtn: false,
                        okFn:function (){
                            wx_redirect('member.html');
                        }
                    });
                    return false;
                }
                $('.error-tips').html(result.datas.msg);
                $('.error-tips').show();

            }
        });
    });


    //发送验证码
    $('.wx_user_send_code').click(function(){
        var user_mobile = $('.wx_user_mobile').val();
        var mobile_code = $('.wx_mobile_code').val();
        if(user_mobile == "" || user_mobile == undefined || user_mobile == null || !reg.test(user_mobile)){
            $('#wx_msg').html('手机号不可为空或格式错误');
            $('#wx_msg').show();
            return false;
        }

        if($('.wx_user_send_code').attr('st')==1) {
            //发送验证码操作
            $.ajax({
                type:'post',
                url:ApiUrl+'/index.php?act=member_index&op=sendMobileCode',
                data:{'mobile':user_mobile,key:key},
                dataType:'json',
                success:function(result){

                    if(result.datas.status==3){
                        if($('.wx_user_send_code').attr('st')==1) {
                            time($('.wx_user_send_code'));
                        }
                        return false;
                    }
                    $('#wx_msg').html(result.datas.msg);
                    $('#wx_msg').show();
                }
            });
        }


    });

    //隐藏消息提示
    $('input').click(function(){
        $('#wx_msg').hide();
    })

    var wait = 120;
    function time(o) {
        if (wait == 0) {
            o.html('重新获取验证码');
            o.attr('st',1);
            wait = 120;
        } else {
            o.html("发送中...(" + wait + ")");
            o.attr('st',2);
            wait--;
            setTimeout(function () {
                    time(o)
                },
                1000)
        }
    }
});