$(function(){
    var memberHtml = '<a class="btn mr5" href="'+WapSiteUrl+'/tmpl/member/member.html?act=member">个人中心</a>';
    var tmpl = '<div class="footer">'
        +'<div class="footer-top">'
            +'<div class="footer-tleft">'+ memberHtml +'</div>'
            +'<a href="javascript:void(0);"class="gotop">'
                +'<span class="gotop-icon"></span>'
                +'<p>回顶部</p>'
            +'</a>'
        +'</div>'
        +'<div class="footer-content">'
            +'<p class="link">'
                +'<a href="javascript:void(0);" class="standard">标准版</a>'
                +'<a href="javascript:void(0);">下载Android客户端</a>'
            +'</p>'
            +'<p class="copyright">'
                +'版权所有 2013-2014 © 江豆网络科技有限公司'
            +'</p>'
        +'</div>'
    +'</div>';
	var render = template.compile(tmpl);
	var html = render();
	$("#footer").html(html);
    //回到顶部
    $(".gotop").click(function (){
        $(window).scrollTop(0);
    });
    var key = getcookie('key');
    var banding_mobile = getcookie('mobile_bind');
    var reg = /^1[3-9]\d{9}$/;
    var referurl = document.referrer;//上级网址
    $("input[name=referurl]").val(referurl);

    if(banding_mobile==1){
        wx_redirect(WapSiteUrl+'/index.html');
        return false;
    }

    //提交操作
    $('.wx_user_mobile_confirm').click(function(){
        var user_mobile = $('.wx_user_mobile').val();
        var mobile_code = $('.wx_mobile_code').val();
        if(user_mobile == "" || user_mobile == undefined || user_mobile == null|| !reg.test(user_mobile)){
            $('#wx_msg').html('手机号不可为空或格式错误');
            $('#wx_msg').show();
            return false;
        }
        if(mobile_code == "" || mobile_code == undefined || mobile_code == null){
            $('#wx_msg').html('验证码不可为空');
            $('#wx_msg').show();
            return false;
        }

        //绑定手机
        $.ajax({
            type:'post',
            url:ApiUrl+'/index.php?act=member_index&op=bindMobile',
            data:{'mobile':user_mobile,'mobile_code':mobile_code,key:key},
            dataType:'json',
            success:function(result){
                if(result.datas.status==6){
                    addcookie('mobile_bind',1);
                    addcookie('user_mobile',user_mobile);
                    wx_redirect(referurl);
                    return false;
                }
                $('#wx_msg').html(result.datas.msg);
                $('#wx_msg').show();

            }
        });
    });


    //发送验证码
    $('.wx_user_send_code').click(function(){
        var user_mobile = $('.wx_user_mobile').val();
        var mobile_code = $('.wx_mobile_code').val();
        if(user_mobile == "" || user_mobile == undefined || user_mobile == null || !reg.test(user_mobile)){
            $('#wx_msg').html('手机号不可为空或格式错误');
            $('#wx_msg').show();
            return false;
        }

        if($('.wx_user_send_code').attr('st')==1) {
            //发送验证码操作
            $.ajax({
                type:'post',
                url:HostUrl+ApiUrl+'/index.php?act=member_index&op=sendMobileCode',
                data:{'mobile':user_mobile,key:key},
                dataType:'json',
                success:function(result){

                    if(result.datas.status==3){
                        var tmp=$('.wx_user_send_code').attr('st');
                        if($('.wx_user_send_code').attr('st')==1) {
                            $('.wx_user_send_code').attr('st',2);
                            time($('.wx_user_send_code'));
                        }
                        return false;
                    }
                    $('#wx_msg').html(result.datas.msg);
                    $('#wx_msg').show();
                    $('.wx_user_send_code').attr('st',1);
                },
                error:function(){
                    alert('the server error');
                }
            });
        }


    });

    //隐藏消息提示
    $('input').click(function(){
        $('#wx_msg').hide();
    })

    var wait = 120;
    function time(o) {
        if (wait == 0) {
            o.html('重新获取验证码');
            o.attr('st',1);
            wait = 120;
        } else {
            o.html("发送中...(" + wait + ")");
            o.attr('st',2);
            wait--;
            setTimeout(function () {
                    time(o)
                },
                1000)
        }
    }
});