<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8"><?php
    $store_info = json_decode(file_get_contents('http://'.$_SERVER['HTTP_HOST'].'/weixin?act=store_index&op=store_info_ajax&store_id='.$_GET['store_id']),true);
    ?><title><?php echo $store_info && isset($store_info['datas']['store_name'])?$store_info['datas']['store_name']:'欢迎光临微享购商城';?></title>

	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" type="text/css" href="css/reset.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="css/widget_menu.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.min.css">
</head>
<body>
<header class="main">
	<!--<div class="header-wrap">-->
		<!--<div class="htsearch-wrap with-home-logo">-->
			<!--<input type="text" class="htsearch-input clr-999" value="" id="keyword" placeholder="搜索besonit商品"/>-->
			<!--<a href="javascript:void(0);" class="search-btn"></a>-->
		<!--</div>-->
	<!--</div>-->
</header>
<div class="main" id="main-container"></div>
<footer id="footer"></footer>
<div data-role="widget" id="widget_wrap" data-widget="menu_4" class="menu_4"></div>
<script type="text/html" id="goods">
	<div class="index_block goods">
		<div class="content">
		<% for (var i in goods_list) { %>
			<div class="goods-item">
				<a href="tmpl/product_detail.html?goods_id=<%= goods_list[i].goods_id %>">
					<div class="goods-item-pic"><img src="<%= goods_list[i].goods_image_url %>" alt=""></div>
					<div class="goods-item-name"><%= goods_list[i].goods_name %></div>
					<div class="goods-item-price">￥<%= goods_list[i].goods_promotion_price %>
                        <% if (goods_list[i].profit != 0) { %>
                        <span class="goods-item-gift"><%= goods_list[i].profit %>元红包</span>
                        <% }%>
                    </div>
				</a>
			</div>
		<% } %>
		</div>
	</div>
</script>
<script type="text/javascript" src="js/config.js"></script>
<script type="text/javascript" src="js/zepto.min.js"></script>
<script type="text/javascript" src="js/template.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/wx_common.js"></script>
<script type="text/javascript" src="js/swipe.js"></script>
<script type="text/javascript" src="js/index.js"></script>
<script src="js/jweixin-1.0.0.js"></script>
<script type="text/javascript" src="js/tmpl/footer.js"></script>
<script type="text/javascript" src="js/footer_menu.js"></script>
<script>
    $(function(){
        var share_title = '微享购';
        var share_desc = '';
        var share_link = changeURLParam(location.href.split('#')[0],'rec',getcookie('share'));
        var share_imgUrl = '';
        wx.ready(function(){

            wx.onMenuShareTimeline({
                title: share_title, // 分享标题
                link: share_link, // 分享链接
                imgUrl: share_imgUrl, // 分享图标
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });

            wx.onMenuShareAppMessage({
                title: share_title, // 分享标题
                desc: share_desc, // 分享描述
                link: share_link, // 分享链接
                imgUrl: share_imgUrl, // 分享图标
                type: '', // 分享类型,music、video或link，不填默认为link
                dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });
        });
        dealOptionMenus();
    });
</script>
</body>
</html>
