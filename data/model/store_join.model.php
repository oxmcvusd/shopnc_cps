<?php
/**
 * 店铺首页产品管理
 *
 *
 *
 */
defined('BYshopJL') or exit('Access Invalid!');

class store_joinModel extends Model
{

    public function __construct()
    {
        parent::__construct('bs_store_join');
    }


    /**
     * getApplyList
     * 获取申请列表
     * @param $condition
     * @param string $field
     * @param int $page
     * @param string $order
     * @param int $limit
     * @param int $count
     * @return mixed
     */
    public function getApplyList($condition, $field = '*', $page = 0, $order = 'dateline desc', $limit = 0, $count = 0)
    {
        return $this->table('bs_store_join')->field($field)->where($condition)->order($order)->limit($limit)->page($page, $count)->select();
    }


    /**
     * getOnceApply
     * 获取单挑申请信息
     * @param $condition
     * @param string $field
     * @return mixed
     */
    public function getOnceApply($condition, $field = '*')
    {

        return $this->where($condition)->field($field)->find();
    }

    /**
     * updateApply
     * 更新信息
     * @param $condition
     * @param $data
     * @return bool
     */
    public function updateApply($condition, $data)
    {
        $rs = $this->where($condition)->update($data);
        if ($rs) return true;
        return false;
    }

    /**
     * Apply
     * 插入数据
     * @param $data
     * @return bool
     */
    public function addApply($data)
    {
        $rs = $this->insert($data);
        if ($rs) return true;
        return false;
    }
}
