/* 商品表添加佣金和红包*/
ALTER TABLE `shopjl_goods`
  ADD COLUMN `brokerage` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享佣金' AFTER `evaluation_good_star`,
  ADD COLUMN `profit` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '红包' AFTER `brokerage` ;
/* 订单增加佣金红包相关字段*/
ALTER TABLE `shopjl_order`
  ADD COLUMN `brokerage` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享佣金总额' AFTER `shipping_code`,
  ADD COLUMN `profit` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '红包总额' AFTER `brokerage`,
  ADD COLUMN `brokerage_account` INT (11) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享会员id' AFTER `profit`,
  ADD COLUMN `profit_account` INT (11) UNSIGNED NOT NULL COMMENT '红包会员id' AFTER `brokerage_account` ;
ALTER TABLE `shopjl_order_goods`
  ADD COLUMN `brokerage` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享佣金总额',
  ADD COLUMN `profit` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '红包总额' AFTER `brokerage`,
  ADD COLUMN `brokerage_account` INT (11) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享会员id' AFTER `profit`,
  ADD COLUMN `profit_account` INT (11) UNSIGNED NOT NULL COMMENT '红包会员id' AFTER `brokerage_account` ;

/*会员表增加微信相关信息*/
ALTER TABLE `shopjl_member`
  ADD COLUMN `member_wxopenid` VARCHAR (40) NULL COMMENT '微信openid' AFTER `member_qqinfo`,
  ADD COLUMN `member_wxinfo` TEXT NULL COMMENT '微信信息' AFTER `member_wxopenid`,
  ADD COLUMN `member_wxunionid` VARCHAR (40) NULL COMMENT '微信unionid' AFTER `member_wxinfo` ;
  ADD COLUMN `recommend_id` INT(11) UNSIGNED DEFAULT 0 NOT NULL COMMENT '推荐人id' AFTER `member_points`,
  ADD COLUMN `share_sign` CHAR(32) NOT NULL COMMENT '分享标识' AFTER `recommend_id`;

/*分享佣金账户表*/
CREATE TABLE `shopjl_bs_brokerage` (
  `bk_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '自增编号',
  `bk_member_id` INT(11) NOT NULL COMMENT '会员编号',
  `bk_member_name` VARCHAR(50) NOT NULL COMMENT '会员名称',
  `bk_amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `bk_bank_name` VARCHAR(40) NOT NULL COMMENT '收款银行',
  `bk_bank_no` VARCHAR(30) DEFAULT NULL COMMENT '收款账号',
  `bk_bank_user` VARCHAR(10) DEFAULT NULL COMMENT '开户人姓名',
  `bk_id_number` CHAR(18) DEFAULT NULL COMMENT '身份证号',
  PRIMARY KEY (`bk_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='分享佣金表';

/*佣金/红包记录*/
CREATE TABLE `shopjl_bs_profit_log` (
  `profit_log_id` BIGINT NOT NULL AUTO_INCREMENT,
  `order_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单',
  `goods_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品',
  `store_id` INT (11) UNSIGNED NOT NULL COMMENT '店铺',
  `member_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id(受益人)',
  `type` TINYINT (1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1佣金,2红包',
  `amount` DECIMAL (10, 2) NOT NULL DEFAULT 0 COMMENT '总金额',
  `note` VARCHAR (200) NOT NULL COMMENT '备注',
  `modified` INT (10) UNSIGNED NOT NULL COMMENT '修改时间',
  `dateline` INT (10) UNSIGNED NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`profit_log_id`)
) ENGINE = INNODB CHARSET = utf8 COLLATE = utf8_general_ci COMMENT = '佣金/红包记录' ;

/* 佣金提现申请*/
CREATE TABLE `shopjl_bs_brokerage_apply` (
  `apply_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `member_id` INT (11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '申请人id',
  `amount` DECIMAL (10, 2) NOT NULL DEFAULT 0 COMMENT '申请金额',
  `apply_time` INT (10) UNSIGNED NOT NULL COMMENT '申请时间',
  `bk_bank_name` VARCHAR (40) NOT NULL DEFAULT '' COMMENT '银行名称',
  `bk_bank_no` VARCHAR (30) NOT NULL DEFAULT '' COMMENT '账号',
  `bk_bank_user` VARCHAR (10) NOT NULL DEFAULT '' COMMENT '开户姓名',
  `verify_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '审核人编号',
  `verify_time` INT (10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '审核时间',
  `transfer_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '转账人编号',
  `transfer_time` INT (10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '转账时间',
  `transfer_no` VARCHAR (40) NOT NULL DEFAULT '' COMMENT '转账流水号',
  `status` TINYINT NOT NULL DEFAULT 0 COMMENT '状态0申请中,1已审核,-1已驳回',
  `modified` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  `dateline` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`apply_id`)
) ENGINE = INNODB CHARSET = utf8 COLLATE = utf8_general_ci COMMENT = '佣金提现申请';

CREATE TABLE `shopjl_bs_brokerage_apply_log` (
  `log_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `apply_id` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '申请id',
  `status` TINYINT NOT NULL DEFAULT 0 COMMENT '当前申请状态',
  `op_type` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '操作人类型0普通用户,1管理员,',
  `op_id` INT NOT NULL DEFAULT 0 COMMENT '操作人id',
  `amount` DECIMAL (10, 2) NOT NULL DEFAULT 0 COMMENT '影响金额',
  `note` VARCHAR (200) NOT NULL DEFAULT '' COMMENT '备注',
  `dateline` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`log_id`)
) ENGINE = INNODB CHARSET = utf8 COLLATE = utf8_general_ci ;

/*商品浏览量*/
CREATE TABLE `shopjl_bs_goods_view` (
  `goods_id` INT (10) NOT NULL COMMENT '商品编号',
  `year_month` INT (10) NOT NULL COMMENT '年月',
  `month_view` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '月浏览量',
  `d01` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d02` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d03` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d04` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d05` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d06` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d07` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d08` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d09` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d10` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d11` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d12` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d13` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d14` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d15` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d16` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d17` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d18` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d19` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d20` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d21` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d22` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d23` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d24` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d25` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d26` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d27` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d28` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d29` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d30` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d31` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  PRIMARY KEY (`goods_id`, `year_month`)
) ENGINE = MYISAM CHARSET = utf8 COLLATE = utf8_general_ci COMMENT = '商品浏览量' ;

/*分享浏览量*/
CREATE TABLE `shopjl_bs_share_view` (
  `member_id` INT (11) NOT NULL DEFAULT 0 COMMENT '商品编号',
  `goods_id` INT (11) NOT NULL DEFAULT 0 COMMENT '商品编号',
  `year_month` INT (10) NOT NULL COMMENT '年月',
  `url_md5` CHAR (32) NOT NULL DEFAULT '' COMMENT '分享链接md5值',
  `url` VARCHAR (100) NOT NULL DEFAULT '' COMMENT '分享链接',
  `month_view` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '月浏览量',
  `d01` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d02` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d03` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d04` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d05` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d06` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d07` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d08` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d09` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d10` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d11` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d12` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d13` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d14` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d15` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d16` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d17` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d18` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d19` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d20` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d21` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d22` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d23` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d24` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d25` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d26` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d27` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d28` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d29` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d30` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d31` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  PRIMARY KEY (`member_id`, `year_month`,`url_md5`,`goods_id`)
) ENGINE = MYISAM CHARSET = utf8 COLLATE = utf8_general_ci COMMENT = '分享浏览量' ;


/*goods_common 表增加分享佣金，红包*/
ALTER TABLE `shopjl_goods_common`
ADD COLUMN `brokerage` DECIMAL(10,2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享佣金' AFTER `goods_serial`,
ADD COLUMN `profit` DECIMAL(10,2) UNSIGNED NOT NULL COMMENT '红包' AFTER `brokerage`;

/*cart 购物车表加分享佣金，红包*/
ALTER TABLE `shopjl_cart` ADD COLUMN `goods_profit` DECIMAL(10,2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '单件商品抵扣红包' AFTER `bl_id`,
ADD COLUMN `goods_brokerage` DECIMAL(10,2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '单件商品分享获得奖励' AFTER `goods_profit`;

/*order 添加佣金发放时间 zwj 20150413*/
ALTER TABLE `shopjl_order` ADD COLUMN `brokerage_time` INT(10) UNSIGNED DEFAULT 0 NOT NULL COMMENT '佣金发放时间' AFTER `profit_account`;
/* 佣金红包记录店铺id设置默认值 201504171509: zwj*/
ALTER TABLE `shopjl_bs_profit_log` CHANGE `store_id` `store_id` INT(11) UNSIGNED DEFAULT 0 NOT NULL COMMENT '店铺';

/*虚拟订单表*/
ALTER TABLE `shopjl_vr_order`
  ADD COLUMN `brokerage` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享佣金总额' AFTER `use_state`,
  ADD COLUMN `profit` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '红包总额' AFTER `brokerage`,
  ADD COLUMN `brokerage_account` INT (11) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享会员id' AFTER `profit`,
  ADD COLUMN `profit_account` INT (11) UNSIGNED NOT NULL COMMENT '红包会员id' AFTER `brokerage_account` ;

/* 添加佣金账户添加佣金累计总收入和冻结金额字段 zwj 201504251522*/
ALTER TABLE `shopjl_bs_brokerage` ADD COLUMN `bk_total` DECIMAL(10,2) DEFAULT 0.00 NOT NULL COMMENT '累计佣金金额' AFTER `bk_member_name`,
 ADD COLUMN `bk_blocking_apply` DECIMAL(10,2) DEFAULT 0.00 NOT NULL COMMENT '冻结中的佣金金额' AFTER `bk_amount`;
/* 添加佣金账户添加预计收益字段 zwj 201504270431*/
 ALTER TABLE `shopjl_bs_brokerage` ADD COLUMN `bk_anticipated` DECIMAL(10,2) DEFAULT 0.00 NOT NULL COMMENT '预计收益' AFTER `bk_blocking_apply`;



 /*用户分享浏览量*/
CREATE TABLE `shopjl_bs_member_share_view` (
  `member_id` INT (11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `year_month` INT (10) NOT NULL COMMENT '年月',
  `month_view` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '月浏览量',
  `d01` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d02` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d03` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d04` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d05` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d06` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d07` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d08` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d09` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d10` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d11` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d12` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d13` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d14` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d15` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d16` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d17` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d18` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d19` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d20` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d21` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d22` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d23` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d24` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d25` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d26` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d27` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d28` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d29` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d30` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d31` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  PRIMARY KEY (`member_id`, `year_month`)
) ENGINE = MYISAM CHARSET = utf8 COLLATE = utf8_general_ci COMMENT = '用户分享浏览量' ;

/* 添加货到付款 zwj 201505051626*/
INSERT INTO `shopjl_mb_payment` (`payment_id`, `payment_code`, `payment_name`, `payment_config`, `payment_state`) VALUES ('3', 'offline', '货到付款', '', '1');

/* 增加商品的分享说明字段 zwj 201505061121*/
ALTER TABLE `shopjl_goods_common` ADD COLUMN `share_note` VARCHAR(500) DEFAULT '' NOT NULL COMMENT '分享说明' AFTER `profit`;

ALTER TABLE `shopjl_goods_common` ADD COLUMN `share_title` VARCHAR(100) DEFAULT '' NOT NULL COMMENT '分享标题' AFTER `profit`;


/*用户分享量 zwj*/
CREATE TABLE `shopjl_bs_member_share_count` (
  `member_id` INT (11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `year_month` INT (10) NOT NULL COMMENT '年月',
  `month_view` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '月分享量',
  `d01` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d02` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d03` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d04` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d05` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d06` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d07` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d08` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d09` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d10` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d11` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d12` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d13` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d14` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d15` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d16` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d17` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d18` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d19` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d20` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d21` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d22` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d23` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d24` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d25` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d26` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d27` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d28` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d29` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d30` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  `d31` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日分享量(数字是日期)',
  PRIMARY KEY (`member_id`, `year_month`)
) ENGINE = MYISAM CHARSET = utf8 COLLATE = utf8_general_ci COMMENT = '用户分享量' ;ALTER TABLE `shopjl_goods_common` ADD COLUMN `share_note` VARCHAR(500) DEFAULT '' NOT NULL COMMENT '分享说明' AFTER `profit`;



/**
 微享购手机端开启 货到付款 需处理表
 *
 *
 * 1. shopjl_mb_payment 需增加一条货到付款记录
 * INSERT INTO `shopjl_mb_payment` (`payment_id`, `payment_code`, `payment_name`, `payment_config`, `payment_state`) VALUES ('3', 'offline', '货到付款', '', '1');
 * 2. 店铺需开启货到付款
 * update `shopjl_store` set `store_huodaofk` = 1 where `store_id` = ;
 * 3. 货到付款设置成功后需设置针对地区的货到付款（如若未设置，货到付款依然不可用）
 *   需修改表 shopjl__offpay_area
 */

 /*
  自定义商品属性
  */
  ALTER TABLE `shopjl_goods_common` ADD COLUMN `new_attr` TEXT NULL COMMENT '自定义商品属性和值' AFTER `share_note`;



  /*
  2015/5/21
  商户微网站首页配置表
  */
  CREATE TABLE `besonit_bs_store_wx_index`(
  `store_id` INT UNSIGNED NOT NULL COMMENT '商店id',
  `goods_id_list` TEXT COMMENT '商品id串',
   PRIMARY KEY (`store_id`)
   ) ENGINE=MYISAM CHARSET=utf8 COLLATE=utf8_general_ci;

   /*会员表增加store_id  2015/5/25*/
   ALTER TABLE `besonit_member` ADD COLUMN `store_id` INT UNSIGNED NOT NULL COMMENT '会员所属商户id' AFTER `member_id`;


   /*微信端申请入驻商家*/
   CREATE TABLE `besonit_bs_store_join`(
   `username` VARCHAR(20) NOT NULL COMMENT '申请者姓名',
    `tel` CHAR(11) NOT NULL COMMENT '手机号',
    `dateline` INT(10) NOT NULL COMMENT '申请日期',
    `modified` INT(10) NOT NULL COMMENT '处理日期',
    `status` TINYINT NOT NULL DEFAULT 10 COMMENT '处理状态，10==未处理，20==已处理',
    PRIMARY KEY (`tel`)
    ) ENGINE=MYISAM CHARSET=utf8 COLLATE=utf8_general_ci;

    ALTER TABLE `besonit_bs_store_join`
    ADD COLUMN `id` INT NOT NULL AUTO_INCREMENT FIRST,
    ADD COLUMN `company` VARCHAR(80) DEFAULT '' NULL COMMENT '公司名称' AFTER `status`,
    ADD COLUMN `address` VARCHAR(80) DEFAULT '' NULL COMMENT '公司地址' AFTER `company`,
    ADD COLUMN `goods_type` VARCHAR(50) DEFAULT '' NULL COMMENT '经营类型' AFTER `address`,
    ADD KEY(`id`), DROP PRIMARY KEY, ADD PRIMARY KEY (`tel`, `id`);