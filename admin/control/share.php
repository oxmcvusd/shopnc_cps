<?php
/**
 * 分享记录查询
 *
 */

defined('BYshopJL') or exit('Access Invalid!');

class shareControl extends SystemControl
{
    public function __construct()
    {
        parent::__construct();
        $add_time_to = date("Y-m-d");
        $time_from = array();
        $time_from['7'] = strtotime($add_time_to) - 60 * 60 * 24 * 7;
        if (empty($_GET['add_time_from']) || $_GET['add_time_from'] > $add_time_to) {//默认显示7天内数据
            $_GET['add_time_from'] = date("Y-m-d", $time_from['7']);
        }
        if (empty($_GET['add_time_to']) || $_GET['add_time_to'] > $add_time_to) {
            $_GET['add_time_to'] = $add_time_to;
        }
    }

    /**
     * 会员分享统计
     */
    public function indexOp()
    {

        $share = Model('goods_share_view');
        $f_key = trim($_GET['f_key']);
        $condition = array();
        if (!empty($f_key)) {
            $condition['member.member_mobile|bs_share_view.member_id'] = array(array('like', '%' . $f_key . '%'));
        }

        $condition['bs_share_view.year_month'] = array('between', intval(strtotime(trim($_GET['add_time_from']))) . ',' . intval(strtotime(trim($_GET['add_time_to']))));

        $share_result = $share->getShareList($condition);
        Tpl::output('show_page', $share->showpage());
        Tpl::output('share_list', $share_result);
        Tpl::showpage('share.list');
    }

    /**
     * profitOp
     * 会员分享收益统计
     */
    public function profitOp(){
        global $config;
        $f_key = trim($_GET['f_key']);
        $curpage = isset($_GET['curpage']) && !empty($_GET['curpage']) ? intval($_GET['curpage']) : 1;
        pagecmd('setEachNum', 10);
        if (!empty($f_key)) {
            $sql_start = "SELECT * FROM ( ";
            $sql= "SELECT v.member_id,SUM(v.month_view) AS all_view,COUNT(*) AS share_count,m.member_mobile FROM {$config['tablepre']}bs_share_view AS v INNER JOIN {$config['tablepre']}member AS m ON v.member_id = m.member_id WHERE v.year_month BETWEEN ".intval(strtotime(trim($_GET['add_time_from'])))." and ".intval(strtotime(trim($_GET['add_time_to']))) ." and (v.`member_id` like '%{$f_key}%' or m.`member_mobile` like '%{$f_key}%') GROUP BY v.`member_id`,m.`member_mobile`)";
            $sql.= "AS new_t LEFT JOIN (SELECT COUNT(*) AS trade_time ,SUM(brokerage) AS all_brokerage ,brokerage_account FROM `besonit_order_goods` GROUP BY brokerage_account) as og ON og.`brokerage_account`= new_t.`member_id`";
            $count_sql = "SELECT count(*) FROM ( ".$sql_start.$sql.") as t";
            $list_sql = $sql_start.$sql." LIMIT ".($curpage-1)*10 .",10";
            $totalNum = Model()->query($count_sql);
            $share_result = Model()->query($list_sql);
            pagecmd('setTotalNum', intval($totalNum[0]['count(*)']));
        }else{
            $sql_start = "SELECT * FROM ( ";
            $sql= "SELECT v.member_id,SUM(v.month_view) AS all_view,COUNT(*) AS share_count,m.member_mobile FROM {$config['tablepre']}bs_share_view AS v INNER JOIN {$config['tablepre']}member AS m ON v.member_id = m.member_id WHERE v.year_month BETWEEN ".intval(strtotime(trim($_GET['add_time_from'])))." and ".intval(strtotime(trim($_GET['add_time_to']))) ." GROUP BY v.`member_id`,m.`member_mobile`)";
            $sql.= "AS new_t LEFT JOIN (SELECT COUNT(*) AS trade_time ,SUM(brokerage) AS all_brokerage ,brokerage_account FROM `besonit_order_goods` GROUP BY brokerage_account) as og ON og.`brokerage_account`= new_t.`member_id`";
            $count_sql = "SELECT count(*) FROM ( ".$sql_start.$sql.") as t";
            $list_sql = $sql_start.$sql." LIMIT ".($curpage-1)*10 .",10";
            $totalNum = Model()->query($count_sql);
            $share_result = Model()->query($list_sql);
            pagecmd('setTotalNum', intval($totalNum[0]['count(*)']));
        }
        Tpl::output('share_list', $share_result);
        Tpl::output('show_page', Model()->showpage());
        Tpl::showpage('share.profit');
    }

    public function index2Op()
    {
        global $config;
        $f_key = trim($_GET['f_key']);
        $curpage = isset($_GET['curpage']) && !empty($_GET['curpage']) ? intval($_GET['curpage']) : 1;
        pagecmd('setEachNum', 10);
        if (!empty($f_key)) {
            $sql_start = "SELECT g.*,sum(v.`month_view`) as `month_view` FROM ( ";
            $sql= "SELECT `goods_id`,`goods_name`,sum(`goods_pay_price`) as all_goods_price,count(*) as order_count,sum(`brokerage`) as all_brokerage FROM {$config['tablepre']}order_goods WHERE `goods_name` like '%{$f_key}%' GROUP BY `goods_id`) ";
            $sql.= "AS g LEFT JOIN {$config['tablepre']}bs_goods_view AS v ON g.`goods_id` = v.`goods_id` AND v.`year_month`>".intval(strtotime(trim($_GET['add_time_from'])));
            $sql.= " AND v.`year_month`<=".intval(strtotime(trim($_GET['add_time_to'])));
            $sql_end= " GROUP BY g.`goods_id`";
            $count_sql = "SELECT count(*) FROM ( ".$sql_start.$sql.$sql_end.") as t";
            $list_sql = $sql_start.$sql.$sql_end." LIMIT ".($curpage-1)*10 .",10";
            $totalNum = Model()->query($count_sql);
            $share_result = Model()->query($list_sql);
            pagecmd('setTotalNum', intval($totalNum[0]['count(*)']));
        }else{
            $sql_start = "SELECT g.*,sum(v.`month_view`) as `month_view` FROM ( ";
            $sql= "SELECT `goods_id`,`goods_name`,sum(`goods_pay_price`) as all_goods_price,count(*) as order_count,sum(`brokerage`) as all_brokerage FROM {$config['tablepre']}order_goods  GROUP BY `goods_id`) ";
            $sql.= "AS g LEFT JOIN {$config['tablepre']}bs_goods_view AS v ON g.`goods_id` = v.`goods_id` AND v.`year_month`>".intval(strtotime(trim($_GET['add_time_from'])));
            $sql.= " AND v.`year_month`<=".intval(strtotime(trim($_GET['add_time_to'])));
            $sql_end= " GROUP BY g.`goods_id`";
            $count_sql = "SELECT count(*) FROM ( ".$sql_start.$sql.$sql_end.") as t";
            $list_sql = $sql_start.$sql.$sql_end." LIMIT ".($curpage-1)*10 .",10";
            $totalNum = Model()->query($count_sql);
            $share_result = Model()->query($list_sql);
            pagecmd('setTotalNum', intval($totalNum[0]['count(*)']));
        }
        Tpl::output('share_list', $share_result);
        Tpl::output('show_page', Model()->showpage());
        Tpl::showpage('share.list');
    }
}
