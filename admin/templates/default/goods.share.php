<?php defined('BYshopJL') or exit('Access Invalid!'); ?>

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <h3>商品分享记录</h3>
            <ul class="tab-base">
                <li><a href="JavaScript:void(0);" class="current"><span>商品分享记录明细</span></a></li>
                <li><a href="index.php?act=goods&op=shareRecord"><span>商品分享记录统计</span></a></li>
            </ul>
        </div>
    </div>
    <div class="fixed-empty"></div>
    <form method="get" action="index.php" name="formSearch" id="formSearch">
        <input type="hidden" name="act" value="goods"/>
        <input type="hidden" name="op" value="share"/>
        <table class="tb-type1 noborder search">
            <tbody>
            <tr>
                <th>产品</th>
                <td><input type="text" class="text" name="f_key" value="<?php echo trim($_GET['f_key']); ?>"
                           placeholder="编号、名称"/></td>
                <th><label for="add_time_from"><?php echo '起止日期'; ?></label></th>
                <td><input class="txt date" type="text" value="<?php echo $_GET['add_time_from']; ?>" id="add_time_from"
                           name="add_time_from">
                    <label for="add_time_to">~</label>
                    <input class="txt date" type="text" value="<?php echo $_GET['add_time_to']; ?>" id="add_time_to"
                           name="add_time_to"/></td>
                <td><a href="javascript:void(0);" id="ncsubmit" class="btn-search "
                       title="<?php echo $lang['nc_query']; ?>">&nbsp;</a></td>
            </tr>
            </tbody>
        </table>
    </form>
    <table class="table tb-type2" id="prompt">
        <tbody>
        <tr class="space odd">
            <th colspan="12">
                <div class="title">
                    <h5><?php echo $lang['nc_prompts']; ?></h5>
                    <span class="arrow"></span></div>
            </th>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>分享统计</li>
                    <li>为使查询信息更准确，请输入聊天双方的完整会员名——登录账号。</li>
                </ul>
            </td>
        </tr>
        </tbody>
    </table>
    <div class="chat-log">
        <table class="table tb-type2 nobdb">
            <thead>
            <tr class="thead">
                <th class="align-center">商品编号</th>
                <th class="align-center">商品名称</th>
                <th class="align-center">订单编号</th>
                <th class="align-center">分享人</th>
                <th class="align-center">浏览人</th>
                <th class="align-center">是否成交</th>
                <th class="align-center">红包/佣金</th>
            </tr>
            <tbody>
            <?php if (is_array($output['share_list']) && !empty($output['share_list'])) { ?>
                <?php foreach ($output['share_list'] as $key => $val) { ?>
                    <tr class="hover member">
                        <td class="w100 align-center"><?php echo $val['goods_id']; ?></td>
                        <td class="align-center"><?php echo $val['goods_name']; ?></td>
                        <td class="align-center"><?php echo $val['order_sn']; ?></td>
                        <td class="align-center"><?php echo $val['share_name']; ?></td>
                        <td class="align-center"><?php echo $val['buyer_name']; ?></td>
                        <td class="align-center"><?php echo  $val['order_state']>=20 ? '是':'否'; ?></td>
                        <td class="align-center"><?php echo $val['brokerage']; ?></td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr class="no_data">
                    <td colspan="5"><?php echo $lang['nc_no_record'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php if (is_array($output['share_list']) && !empty($output['share_list'])) { ?>
            <div class="pagination"><?php echo $output['show_page']; ?></div>
        <?php } ?>
    </div>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL; ?>/js/jquery-ui/jquery.ui.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL; ?>/js/jquery-ui/i18n/zh-CN.js"
        charset="utf-8"></script>
<link rel="stylesheet" type="text/css"
      href="<?php echo RESOURCE_SITE_URL; ?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"/>
<script type="text/javascript">
    $(function () {
        $('#add_time_from').datepicker({dateFormat: 'yy-mm-dd'});
        $('#add_time_to').datepicker({dateFormat: 'yy-mm-dd'});
        $('#ncsubmit').click(function () {
            $('#formSearch').submit();
        });
    });
</script>
