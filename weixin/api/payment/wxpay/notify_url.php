<?php
/**
 * 微信支付通知地址
 *
 * 
 * @copyright  Copyright (c) 2007-2013 BesonIT Inc. (http://www.BesonIT.net)
 * @license    http://www.BesonIT.net
 * @link       http://www.BesonIT.net
 * @since      File available since Release v1.1
 */
$_GET['act']	= 'payment';
$_GET['op']		= 'wxnotify';
$_GET['payment_code'] = 'wxpay';

//file_put_contents('get.txt',var_export($_GET,true));
//file_put_contents('post.txt',var_export($_POST,true));
//file_put_contents('HTTP_RAW_POST_DATA.txt',$GLOBALS['HTTP_RAW_POST_DATA']);
require_once(dirname(__FILE__).'/../../../index.php');
