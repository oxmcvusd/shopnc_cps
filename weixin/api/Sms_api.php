<?php
defined('BYshopJL') or exit('Access Invalid!');


class Sms_api
{
    public $ch;
    public $msgids;
    public $customer_ID, $account, $password, $host;

    function __construct()
    {
        date_default_timezone_set('PRC'); // 在PHP.ini 中 设置：date.timezone=PRC ， 否则会有时区问题
        global $ch, $msgids;
        // 初始化cURL,cURL 是开源的通讯包,cURL 是一个功能强大的PHP库,在 PHP.ini 中 设置 extension=php_curl.dll
        $ch = curl_init();
        $msgids = array();
    }

    public function init($host,$account,$customer_ID,$pwd){
        $this->host = $host; //'http://xtx.telhk.cn:8888/';
        $this->account = $account;
        $this->customer_ID = $customer_ID;
        $this->password = $pwd;
    }

    /**
     * 根据手机数组生成 手机 标签 ;
     * @param $mobilephone
     * @return string
     */
    function createXmlData_Mobiles($mobilephone){
        $xml = '';
        foreach ($mobilephone as $v) {
            $xml .= <<<EOT
<Mobile>
					 <Number>$v</Number>
					 <Operator>0</Operator>
					 <ServiceNumber>0</ServiceNumber>			
					 <SendNumber>0</SendNumber>
				  </Mobile>
EOT;
        }
        return $xml;
    }

    /**
     * 根据每一条内容产生每条信息的XML
     * @param $mobilephone
     * @param $content
     * @return string
     */
    function createXmlData_Messages($mobilephone, $content)
    {

        global $msgids; // 保存ID  到数组中，返回去的时候使用
        //$this->msgids = array() ;
        $mobiles = $this->createXmlData_Mobiles($mobilephone); // 获取手机号码
        $xml = '';
        $Msg_Id = time() . rand(1000, 999999); // 消息ID ， 必须唯一 , 此 ID 和 每一个手机号码 组成唯一的识别， 将用于发送回执更新
        //array_push( $this->$msgids , $Msg_Id ) ;
        $Submit_Time = date('Y-m-d H:i:s', time());
        // print  "当前时间:" . $Submit_Time . "<br />" ;
        $content = base64_encode($content);
        $xml .= <<<EOT
<Message>
			   	   <Msg_Type>1</Msg_Type>
				   <Msg_Id>$Msg_Id</Msg_Id>
				   <Organ_Id>0</Organ_Id>
				   <User_Id>0</User_Id>
				   <Submit_Time>$Submit_Time</Submit_Time>
				   <Channel_Code>0</Channel_Code>
				   <Msg_Content>$content</Msg_Content>
				   <Priority>2</Priority>
				   <Mobiles>
						$mobiles;
					</Mobiles>
			</Message>		
EOT;

        return $xml;

    }

    /**
     * 根据手机号码(数组)和内容数组，返回XML
     * @param $mobilephone
     * @param $content
     * @param $Customer_ID
     * @param $Password
     * @return string
     */
    function createXmlData($mobilephone, $content, $Customer_ID, $Password){
        $Package_Seq = time() . rand(1000, 999999); // 包的序号 ， 必须是唯一的
        $xml = "<Messages>";
        foreach ($content as $v) {
            $message = $this->createXmlData_Messages($mobilephone, $v); // 每条消息
            $xml .= $message;
        }
        $xml .= "</Messages>";
        // 返回XML
        return <<< EOT
<?xml version="1.0" encoding="gbk" ?>				
			<ShortMessages>
					 <Customer_Id>$Customer_ID</Customer_Id>
                     <Mac_Addr>$Password</Mac_Addr>
					 <Package_Seq>{$Package_Seq}</Package_Seq>
					 <Version>2.0</Version>
					 $xml
      	   </ShortMessages>
EOT;
    }

    /**
     * 根据手机号码(数组)，信息内容（数组），客户ID，密码 获取数据包
     * @param $mobilephone
     * @param $content
     * @param $Customer_ID
     * @param $Password
     * @return string
     */
    function GetXmlData($mobilephone, $content, $Customer_ID, $Password){
        return $this->createXmlData($mobilephone, $content, $Customer_ID, $Password);
    }

    /**
     * 发送到服务器
     * @param $url
     * @param $Customer_ID
     * @param $xmldata
     * @return mixed
     */
    function SendToServer($url, $Customer_ID, $xmldata){
        Global $ch; // 使用外部变量
        $rndnum = rand(1, 9999999); // 为了避免缓存，产生 随机数
        // 连接 URL 地址 ，Customer_Id 是 客户ID = 港澳的客户ID ，客户放在配置文件中读取. 如 102733
        $url = $url . "?rmd=" . $rndnum . "&id=" . $Customer_ID;
        $header[] = "Content-type: text/xml;charset=gbk"; // 定义content-type为xml
        // 进行兼容性的编码
        $data = urlencode(urlencode($xmldata));
        // HTTP 头
        $header[] = "Content-length: " . strlen($data);
        // 设置 URL
        curl_setopt($ch, CURLOPT_URL, $url);
        // 设置 如果成功只将结果返回，不自动输出任何内容。如果失败返回FALSE
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // 设置 HTTP 头
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // 设置发送数据
        // 如果有代理的话设置下代理，如果没有就不设置
        //$proxy="128.1.241.11:80" ;
        // curl_setopt($ch, CURLOPT_PROXY, $proxy);
        $response = curl_exec($ch); // 执行发送过程
        return $response;
    }

    function SendXml($mobile, $msg){
        Global $ch;
        $mobilephone[0] = $mobile;
        $content[0] = $msg;
        $url = $this->host."/GetXmlString.aspx"; //http://220.174.209.178:18988 连接地址，有备用ip为：http://113.59.109.38:18988/GetXmlString.aspx
        // 客户ID
        $Customer_ID = $this->customer_ID;
        // 客户密码
        $Password = $this->password;
        // 生成XML字符串
        $xmldata = $this->GetXmlData($mobilephone, $content, $Customer_ID, $Password);

        // 发送到港澳HTTP服务器，获取返回值
        $response = $this->SendToServer($url, $Customer_ID, $xmldata);
        $retryCount = 0;
        // 这里应该判断下，程序是否发送错误，比如：超时之类，一般都是网络方面的错误，如果发送错误，应当重发此包
        while (curl_errno($ch)) {
           // print curl_error($ch); // 显示发送错误代码
            $response = $this->SendToServer($url, $Customer_ID, $xmldata); // 重发此包
            $retryCount = $retryCount + 1;
            if ($retryCount >= 3) {
                break;
            }
        }
        $parser = xml_parser_create();
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, $response, $values, $tags);
        // 打印输出的数组
        //print_r( $values ) ;
        xml_parser_free($parser);
        $error_code = $values[1]["value"];
        $description = $values[2]["value"]; // 描述
        return '{"status":' . $error_code . ',"data":"' . $description . '"}';
        curl_close($ch); // 关闭curl
    }
}