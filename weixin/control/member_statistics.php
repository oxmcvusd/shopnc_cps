<?php
/**
 * 我的地址
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2013 BesonIT Inc. (http://www.besonit.com)
 * @license    http://www.besonit.com
 * @link       http://www.besonit.com
 * @since      File available since Release v1.1
 */


defined('BYshopJL') or exit('Access Invalid!');

class member_statisticsControl extends wxMemberControl
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 申请日志列表
     */
    public function indexOp()
    {
        $data = array(
            'member_id' => $_SESSION['member_id'],
            'year_view' => 0,
            'year_share' => 0,
            'month_view' => 0,
            'month_share' => 0,
            'today_view' => 0,
            'today_share' => 0,
            'yestoday_view' => 0,
            'yestoday_share' => 0,
            'seven_day_view' => 0,
            'seven_day_share' => 0,
        );

        $curr_day=date('d');

        //浏览量
        //
        /* @var wx_bs_member_share_viewModel $bk_share_model */
        $bk_share_model = Model('wx_bs_member_share_view');
        $tmp = $bk_share_model->get_year("member_id = {$this->member_info['member_id']}");
        if($tmp){
            $data['year_view']=$tmp;
        }

        $tmp = $bk_share_model->get_share("member_id = {$this->member_info['member_id']} AND `year_month` = ".strtotime(date('Y-m')));
        if($tmp) {
            $data['month_view']=$tmp['month_view'];
            $data['today_view']=$tmp['d'.$curr_day];
            if(($yestoday=$curr_day-1)>0){
                $data['yestoday_view']=$tmp['d'.sprintf('%02d',$yestoday)];
            }
            for($i=1;$i<=7;$i++){
                if(($day=$curr_day-$i)>0){
                    $data['seven_day_view'] +=$tmp['d'.sprintf('%02d',$day)];
                }
            }
        }

        //分享量
        /* @var wx_bs_member_share_countModel $share_count_model */
        $share_count_model = Model('wx_bs_member_share_count');
        $tmp = $share_count_model->get_year("member_id = {$this->member_info['member_id']}");
        if($tmp){
            $data['year_share']=$tmp;
        }

        $tmp = $share_count_model->get_count("member_id = {$this->member_info['member_id']} AND `year_month` = ".strtotime(date('Y-m')));
        if($tmp) {
            $data['month_share']=$tmp['month_view'];
            $data['today_share']=$tmp['d'.$curr_day];
            if(($yestoday=$curr_day-1)>0){
                $data['yestoday_share']=$tmp['d'.sprintf('%02d',$yestoday)];
            }
            for($i=1;$i<=7;$i++){
                if(($day=$curr_day-$i)>0){
                    $data['seven_day_share'] +=$tmp['d'.sprintf('%02d',$day)];
                }
            }
        }
        op_data(1,'查询成功',$data);
    }
    /**
     * 申请日志列表
     */
    public function share_listOp()
    {
        /* @var wx_bs_member_share_viewModel $bk_share_model */
        $bk_share_model = Model('wx_bs_member_share_view');
        $tmp = $bk_share_model->get_share("member_id = {$this->member_info['member_id']} AND `year_month` = ".strtotime(date('Y-m')));
        /* @var wx_bs_member_share_countModel $share_count_model */
        $share_count_model = Model('wx_bs_member_share_count');
        $count_tmp = $share_count_model->get_count("member_id = {$this->member_info['member_id']} AND `year_month` = ".strtotime(date('Y-m')));
        if($tmp) {
            $data = array(
                'member_id' => $tmp['member_id'],
                'year_month' => $tmp['year_month'],
                'month_view' => $tmp['month_view'],
                'day_view' => array(),
            );
            $curr_day=date('d');
            foreach ($tmp as $field => $v) {
                $k= preg_replace('/[a-z_]*?/','',$field);
                if(!$k || $k>$curr_day){
                    continue;
                }
                $data['day_view'][]=array(
                    'date'=>$data['year_month']+86400*($k-1),
                    'view_count'=>$v,
                    'share_count'=>($count_tmp && isset($count_tmp[$field]))?$count_tmp[$field]:0,
                    'date_str'=>date('Y-m-d',$data['year_month']+86400*($k-1)));/*'date_str'=>date('Y-m-d',$data['year_month']+86400*($k-1)),*/
            }
            $data['day_view']=array_reverse($data['day_view']);
        }
        output_data(array('data_list' => isset($data)?$data:null));
    }
}
