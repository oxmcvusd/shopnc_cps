<?php
/**
 * 我的商城
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2013 BesonIT Inc. (http://www.besonit.com)
 * @license    http://www.besonit.com
 * @link       http://www.besonit.com
 * @since      File available since Release v1.1
 */



defined('BYshopJL') or exit('Access Invalid!');

class member_indexControl extends wxMemberControl {

	public function __construct(){
		parent::__construct();
	}

    /**
     * 我的商城
     */
	public function indexOp() {
        $member_info = array();
        $member_info['user_name'] = strpos($this->member_info['member_name'],'__wx__')===0?'微信用户':$this->member_info['member_name'];
        $member_info['avator'] = getMemberAvatarForID($this->member_info['member_id']);
        //$member_info['point'] = $this->member_info['member_points'];
        $member_info['point'] = $this->member_info['bk_amount']?$this->member_info['bk_amount']:'0.00';
        //$member_info['predepoit'] = $this->member_info['available_predeposit'];
        $member_info['predepoit'] = $this->member_info['bk_total']?$this->member_info['bk_total']:'0.00';
//        Tpl::output('member_info',$member_info);
//        Tpl::showpage('member','member_layout');
        output_data(array('member_info' => $member_info));
	}

    /**
     * bindMobile
     * 绑定手机
     */
    public function bindMobileOp(){

        $mobile = isset($_POST['mobile']) && !empty($_POST['mobile'])? trim($_POST['mobile']) : null;
        $mobile_code = isset($_POST['mobile_code']) && !empty($_POST['mobile_code'])? trim($_POST['mobile_code']) : null;
        $key = isset($_POST['key']) && !empty($_POST['key'])? trim($_POST['key']) : null;

        if($mobile==null || !preg_match('/^1[3-9]\d{9}$/',$mobile)) output_data(array('status'=>1,'msg'=>'手机号不可为空或格式错误'));
        if($mobile_code==null ) output_data(array('status'=>2,'msg'=>'验证码为空'));
        $model_mb_user_token = Model('wx_mb_user_token');
        $member_token_info = $model_mb_user_token->getMbUserTokenInfo(array('member_id'=>$_SESSION['member_id'],'client_type'=>'weixin'));
        if($member_token_info['token']!=$key) output_data(array('status'=>2,'msg'=>'用户信息错误，非法操作'));
        list($code,$time) = explode('_',$_SESSION['user_binding_mobile_code']);
        if(empty($time) || $code!=$mobile_code) output_data(array('status'=>3,'msg'=>'验证码不正确'));
        if(empty($time) || (time()-intval($time))>120) output_data(array('status'=>4,'msg'=>'验证码已过期'));
        $model_member = Model('wx_member');
        $member_info = $model_member->getMemberInfo(array('member_id' => $_SESSION['member_id']));
        if($member_info['member_mobile_bind']==1)  output_data(array('status'=>5,'msg'=>'此手机号已绑定过'));
        $rs = $model_member->editMember(array('member_id'=>$_SESSION['member_id']),array('member_mobile_bind'=>1,'member_mobile'=>$mobile));
        if($rs){
            $_SESSION['user_binding_mobile_code'] = null;
            output_data(array('status'=>6,'msg'=>'操作成功'));
        }
        output_data(array('status'=>7,'msg'=>'绑定失败，请重试'));
    }

    /**
     * sendMobileCode
     * 发送验证码操作
     */
    public function sendMobileCodeOp(){
        if($_SESSION['user_binding_mobile_code']){
            list($CCode,$TTime) = explode('_',$_SESSION['user_binding_mobile_code']);
            if(time()-$TTime < 120) output_data(array('status'=>1,'msg'=>'不可重复点击'));
        }
        $mobile = isset($_POST['mobile']) && !empty($_POST['mobile'])? trim($_POST['mobile']) : null;
        $key = isset($_POST['key']) && !empty($_POST['key'])? trim($_POST['key']) : null;

        if($mobile==null || !preg_match('/^1[3-9]\d{9}$/',$mobile)) output_data(array('status'=>1,'msg'=>'手机号不可为空或格式错误'));
        $model_mb_user_token = Model('wx_mb_user_token');
        $member_token_info = $model_mb_user_token->getMbUserTokenInfo(array('member_id'=>$_SESSION['member_id'],'client_type'=>'weixin'));
        if($member_token_info['token']!=$key) output_data(array('status'=>2,'msg'=>'用户信息错误，非法操作'));



        //output_data(array('status'=>4,'msg'=>'发送失败，请重试'));

        //生成code
        $code = random(6,1);
        $_SESSION['user_binding_mobile_code'] = $code.'_'.time();
        //调用短信接口
        if(send_SMS($mobile,$code)){
            $massage = array(
                'member_id'     =>$_SESSION['member_id'],
                'to_member_id'  =>$_SESSION['member_id'],
                'msg_title' =>'绑定手机发送验证码',
                'msg_content'  =>$_SESSION['user_binding_mobile_code'],
                'to_member_name' => $_SESSION['member_name'],
                'message_type' =>3//0为私信、1为系统消息、2为留言,3为短信
            );
            record_message($massage);
            output_data(array('status'=>3,'msg'=>'发送成功'));
        }else{
            output_data(array('status'=>4,'msg'=>'发送失败，请重试'));
        }

    }

    /**
     * 分享统计
     * Author: walkskyer
     */
    public function share_countOp()
    {
        //分享浏览量
        /* @var wx_bs_member_share_countModel $model*/
        $model=Model('wx_bs_member_share_count');
        $model->updateCount($_SESSION['member_id']);
    }

}
