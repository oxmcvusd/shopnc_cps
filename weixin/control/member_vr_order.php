<?php
/**
 * 我的订单
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2013 BesonIT Inc. (http://www.besonit.com)
 * @license    http://www.besonit.com
 * @link       http://www.besonit.com
 * @since      File available since Release v1.1
 */


defined('BYshopJL') or exit('Access Invalid!');

class member_vr_orderControl extends wxMemberControl {

	public function __construct(){
		parent::__construct();
	}

    /**
     * 订单列表
     */
    public function order_listOp() {

        $model_vr_order = Model('vr_order');
        
        $condition = array();
        $condition['buyer_id'] = $this->member_info['member_id'];
        $order_list = $model_vr_order->getOrderList($condition, $this->page, '*', 'order_id desc');

        foreach ($order_list as $key => $order) {
            //显示取消订单
            $order_list[$key]['if_cancel'] = $model_vr_order->getOrderOperateState('buyer_cancel',$order);
        
            //显示支付
            $order_list[$key]['if_pay'] = $model_vr_order->getOrderOperateState('payment',$order);

            $order_list[$key]['goods_image_url'] = cthumb($order['goods_image'], 240, $order['store_id']);
        }

        $page_count = $model_vr_order->gettotalpage();

        output_data(array('order_list' => $order_list), mobile_page($page_count));
    }

    public function indate_code_listOp() {
        $order_id = intval($_POST['order_id']);
        if ($order_id <= 0) {
            output_error('订单不存在');
        }
        $model_vr_order = Model('vr_order');
        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['buyer_id'] = $this->member_info['member_id'];
        $order_info = $model_vr_order->getOrderInfo($condition);
        if (empty($order_info) || $order_info['delete_state'] == ORDER_DEL_STATE_DROP) {
            output_error('订单不存在');
        }
        $order_list = array();
        $order_list[$order_id] = $order_info;
        $order_list = $model_vr_order->getCodeRefundList($order_list);//没有使用的兑换码列表
        $code_list = array();
        if(!empty($order_list[$order_id]['code_list'])) {
            foreach ($order_list[$order_id]['code_list'] as $value) {
                $code = array();
                $code['vr_code'] = $value['vr_code'];
                $code['vr_indate'] = $value['vr_indate'];
                $code_list[] = $code;
            }
        }
        output_data(array('code_list' => $code_list));
    }

    /**
     * 取消订单
     */
    public function order_cancelOp() {
        $model_vr_order = Model('vr_order');
        $condition = array();
        $condition['order_id'] = intval($_POST['order_id']);
        $condition['buyer_id'] = $this->member_info['member_id'];
        $order_info	= $model_vr_order->getOrderInfo($condition);

        $if_allow = $model_vr_order->getOrderOperateState('buyer_cancel',$order_info);
        if (!$if_allow) {
            output_data('无权操作');
        }

        $logic_vr_order = Logic('vr_order');
        $result = $logic_vr_order->changeOrderStateCancel($order_info,'buyer', '其它原因');

        if(!$result['state']) {
            output_data($result['msg']);
        } else {
            output_data('1');
        }
    }

    /*
    * 订单详情
    */

    public function order_infoOp()
    {
        $model_vr_order = Model('vr_order');
        $pay_sn = $_POST['pay_sn'];
        $key = $_POST['key'];
        if(!$pay_sn) output_error('支付码错误');
        if(!$key)  output_error('用户信息错误');

        $mb_token = Model('mb_user_token')->getMbUserTokenInfoByToken($key);
        if(!$mb_token) output_error('token错误');
        if($mb_token['member_id'] != $this->member_info['member_id']) output_error('用户信息错误');

        $condition = array();
        $condition['order_sn'] = $pay_sn;
        $condition['buyer_id'] = $this->member_info['member_id'];

        $order_info = $model_vr_order->getOrderInfo($condition);
        if(!$order_info)  output_error('订单信息错误');
        $order_info['goods_image_url'] = cthumb($order_info['goods_image'], 240, $order_info['store_id']);
        //发起微信支付
        $pay_param = self::create_wxpay_param($order_info);
        $order_info['pay_param'] = json_decode($pay_param);
        output_data(array('order_info' => $order_info));
    }

    private function create_wxpay_param($order_info){
        include_once(API_PATH."/payment/wxpay/WxPayPubHelper/WxPayPubHelper.php");

        $jsApi = new JsApi_pub();

        $openid = $this->member_info['member_wxopenid'];
        $out_trade_no = $order_info['order_sn'];
        $total_fee = floatval($order_info['order_amount'])*100;//转换为分
        //使用统一支付接口
        $unifiedOrder = new UnifiedOrder_pub();

        //设置统一支付接口参数
        //设置必填参数
        //appid已填,商户无需重复填写
        //mch_id已填,商户无需重复填写
        //noncestr已填,商户无需重复填写
        //spbill_create_ip已填,商户无需重复填写
        //sign已填,商户无需重复填写
        $unifiedOrder->setParameter("openid","$openid");//商品描述
        $unifiedOrder->setParameter("body","商品描述");//商品描述

        $unifiedOrder->setParameter("out_trade_no","$out_trade_no");//商户订单号
        $unifiedOrder->setParameter("total_fee","$total_fee");//总金额
        $unifiedOrder->setParameter("notify_url",WxPayConf_pub::NOTIFY_URL);//通知地址
        $unifiedOrder->setParameter("trade_type","JSAPI");//交易类型
        //非必填参数，商户可根据实际情况选填
        //$unifiedOrder->setParameter("sub_mch_id","XXXX");//子商户号
        //$unifiedOrder->setParameter("device_info","XXXX");//设备号
        //$unifiedOrder->setParameter("attach","XXXX");//附加数据
        //$unifiedOrder->setParameter("time_start","XXXX");//交易起始时间
        //$unifiedOrder->setParameter("time_expire","XXXX");//交易结束时间
        //$unifiedOrder->setParameter("goods_tag","XXXX");//商品标记
        //$unifiedOrder->setParameter("openid","XXXX");//用户标识
        //$unifiedOrder->setParameter("product_id","XXXX");//商品ID

        $prepay_id = $unifiedOrder->getPrepayId();
        //=========步骤3：使用jsapi调起支付============
        $jsApi->setPrepayId($prepay_id);

        $jsApiParameters = $jsApi->getParameters();
        return $jsApiParameters;
    }
}
