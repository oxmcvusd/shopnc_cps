<?php

defined('BYshopJL') or exit('Access Invalid!');

/********************************** 前台control父类 **********************************************/
class wxControl
{

    //客户端类型
    protected $client_type_array = array('android', 'wap', 'wechat', 'ios','weixin');
    //列表默认分页数
    protected $page = 5;


    public function __construct()
    {

        Language::read('mobile');

        //分页数处理
        $page = intval($_GET['page']);
        if ($page > 0) {
            $this->page = $page;
        }

        /**
         * 未对推荐进行处理
         */


        /*if (ENV == 'dev') {
            //开发版，模拟用户
            $_SESSION['is_login'] = '1';
            $_SESSION['member_id'] = 12;
            $_SESSION['is_buy'] = 1;
            $_SESSION['share_sign'] = '';
            $_SESSION['member_wxopenid'] = '';
            $_SESSION['member_wxunionid'] = '';
        }
        if (ENV == 'pdt') {
            //生产模式，进行微信授权
            if ($_SESSION['is_login'] != 1) {//未登录
                $wx = new weixin();
                if (!isset($_GET['code'])) $wx->webAuthGetCode();
                $result = $wx->webAuthGetAccessToken();
                if (isset($result['errcode'])) showMessage('Code无效');
                $member = Model('wx_member', BASE_PATH);
                //会员注册及登陆
                $member->register($result);
            }
        }*/


    }
}

class wxHomeControl extends wxControl
{
    public function __construct()
    {
        parent::__construct();
    }
}

class wxMemberControl extends wxControl
{

    protected $member_info = array();

    public function __construct()
    {
        parent::__construct();
        Tpl::setDir('member');
        /* @var wx_memberModel $model_member*/
        $model_member = Model('wx_member');
        $this->member_info = $model_member->getMemberInfoByID($_SESSION['member_id']);
        $seller_info = Model('seller')->getSellerInfo(array('member_id' => $this->member_info['member_id']));
        $this->member_info['store_id'] = $seller_info['store_id'];

    }
}
