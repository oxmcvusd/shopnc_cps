<?php
/**
 * 申请入驻商家
 *
 *
 *
 *
 */


defined('BYshopJL') or exit('Access Invalid!');

class store_joinControl extends wxHomeControl
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 登录
     */
    public function indexOp()
    {
        if (empty($_POST['username']) || empty($_POST['tel']) || !in_array($_POST['client'], $this->client_type_array)|| empty($_POST['company'])|| empty($_POST['address'])|| empty($_POST['goods_type'])) {
            output_error('信息不完整，申请失败');
        }

        $store_join = Model('store_join');

        $array = array();
        $array['tel'] = $_POST['tel'];
        $apply_info = $store_join->getOnceApply($array);
        if (!empty($apply_info)) output_error('此手机已申请过,申请失败');
        $data['username'] = trim($_POST['username']);
        $data['tel'] = trim($_POST['tel']);
        $data['company'] = trim($_POST['company']);
        $data['address'] = trim($_POST['address']);
        $data['goods_type'] = trim($_POST['goods_type']);
        $data['dateline'] = time();
        $rs = $store_join->addApply($data);
        if ($rs) output_data(array('status' => 1));
        output_error('申请失败');
    }


}
