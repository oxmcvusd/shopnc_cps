<?php
/**
 * 我的地址
 *
 *
 *
 *
 * @copyright  Copyright (c) 2007-2013 BesonIT Inc. (http://www.besonit.com)
 * @license    http://www.besonit.com
 * @link       http://www.besonit.com
 * @since      File available since Release v1.1
 */


defined('BYshopJL') or exit('Access Invalid!');

class member_addressControl extends wxMemberControl
{
    private $model_address;

    public function __construct()
    {
        parent::__construct();
        $this->model_address = Model('wx_address');
    }

    /**
     * 地址列表
     */
    public function address_listOp()
    {

        $address_list = $this->model_address->getAddressList(array('member_id' => $this->member_info['member_id']));
//        Tpl::output('address_list', $address_list);
//        Tpl::showpage('address_list', 'member_layout');
        output_data(array('address_list' => $address_list));
    }

    /**
     * 地址详细信息
     */
    public function address_infoOp() {
        $address_id = intval($_POST['address_id']);

        $model_address = Model('address');

        $condition = array();
        $condition['address_id'] = $address_id;
        $address_info = $model_address->getAddressInfo($condition);
        if(!empty($address_id) && $address_info['member_id'] == $this->member_info['member_id']) {
            //查询省份id
            $area = Model('area')->getAreaInfo(array('area_id'=>$address_info['city_id']));
            $address_info['prov'] = $area['area_parent_id'];
            output_data(array('address_info' => $address_info));
        } else {
            output_error('地址不存在');
        }
    }
    /**
     * 删除地址
     */
    public function address_delOp()
    {
        $address_id = intval($_POST['address_id']);


        $condition = array();
        $condition['address_id'] = $address_id;
        $condition['member_id'] = $this->member_info['member_id'];
        $this->model_address->delAddress($condition);
        output_data('1');
    }

    /**
     * 新增地址
     */
    public function address_addOp()
    {

        $address_info = $this->_address_valid();

        $result = $this->model_address->addAddress($address_info);
        if ($result) {
            output_data(array('address_id' => $result));
        } else {
            output_error('保存失败');
        }
    }

    /**
     * 编辑地址
     */
    public function address_editOp()
    {
        $address_id = intval($_POST['address_id']);


        //验证地址是否为本人
        $address_info = $this->model_address->getOneAddress($address_id);
        if ($address_info['member_id'] != $this->member_info['member_id']) {
            output_error('参数错误');
        }

        $address_info = $this->_address_valid();

        $result = $this->model_address->editAddress($address_info, array('address_id' => $address_id));
        if ($result) {
            output_data('1');
        } else {
            output_error('保存失败');
        }
    }

    /**
     * 验证地址数据
     */
    private function _address_valid()
    {
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array("input" => $_POST["true_name"], "require" => "true", "message" => '姓名不能为空'),
            array("input" => $_POST["area_info"], "require" => "true", "message" => '地区不能为空'),
            array("input" => $_POST["address"], "require" => "true", "message" => '地址不能为空'),
            array("input" => $_POST['tel_phone'] . $_POST['mob_phone'], 'require' => 'true', 'message' => '联系方式不能为空')
        );
        $error = $obj_validate->validate();
        if ($error != '') {
            output_error($error);
        }

        $data = array();
        $data['member_id'] = $this->member_info['member_id'];
        $data['true_name'] = $_POST['true_name'];
        $data['area_id'] = intval($_POST['area_id']);
        $data['city_id'] = intval($_POST['city_id']);
        $data['area_info'] = $_POST['area_info'];
        $data['address'] = $_POST['address'];
        $data['tel_phone'] = $_POST['tel_phone'];
        $data['mob_phone'] = $_POST['mob_phone'];
        return $data;
    }

    /**
     * 地区列表
     */
    public function area_listOp()
    {
        $area_id = intval($_POST['area_id']);

        $model_area = Model('area');

        $condition = array();
        if ($area_id > 0) {
            $condition['area_parent_id'] = $area_id;
        } else {
            $condition['area_deep'] = 1;
        }
        $area_list = $model_area->getAreaList($condition, 'area_id,area_name');
        output_data(array('area_list' => $area_list));
    }

}
