<?php
/**
 * cms首页
 *
 *
 *
 * @copyright  Copyright (c) 2007-2013 BesonIT Inc. (http://www.BesonIT.net)
 * @license    http://www.BesonIT.net
 * @link       http://www.BesonIT.net
 * @since      File available since Release v1.1
 */


defined('BYshopJL') or exit('Access Invalid!');

class store_indexControl extends wxHomeControl
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 首页
     */
    public function indexOp()
    {
        $model_goods = Model('goods');
        $model_wx_index = Model('store_wx_index');
        $map['store_id'] = $_GET['store_id'];
        if (!$map['store_id']) output_error('参数错误');
        $goods_id_array = $model_wx_index->getStoreIndexGoodsId($map);
        $where = array();
        $where['store_id'] = $map['store_id'];
        $where['goods_id'] = array('in', $goods_id_array);
        $goods_list = $model_goods->getGoodsOnlineList($where, '*');
        if (empty($goods_list)) {
            unset($where['goods_id']);
            $goods_list = $model_goods->getGoodsOnlineList($where, '*', 10);
            if (empty($goods_list)) output_error('暂无商品');
        }
        foreach ($goods_list as $k => $v) {
            $goods_list[$k]['goods_image_url'] = cthumb($v['goods_image']);
            $goods_list[$k]['profit'] = trimz($v['profit']);
        }
        $store_info = Model('store')->getStoreInfo($map);
        output_data(array('goods_list' => $goods_list,'store_info'=>$store_info));
    }

    /**
     * 通过ajax获取店铺信息
     * Author: walkskyer
     * Email:zwj_work@qq.com
     */
    public function store_info_ajaxOp(){
        if(!isset($_GET['store_id']) || empty($_GET['store_id']) || $_GET['store_id']<0){
            op_error(-2,'没有数据');
        }
        /* @var storeModel $model_store*/
        $model_store = Model('store');
        $store_info_tmp = $model_store->getStoreInfoByID($_GET['store_id']);
        $store_info=array(
            'store_id'=>$store_info_tmp['store_id'],
            'store_name'=>$store_info_tmp['store_name'],
        );
        op_success(1,'查询成功',$store_info);
    }

}
