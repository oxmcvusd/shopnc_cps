<?php



defined('BYshopJL') or exit('Access Invalid!');
class articleControl extends wxHomeControl{

	public function __construct() {
        parent::__construct();
    }


	public function indexOp() {

        $article = Model('article');
        $id = $_GET['id'];
        if(empty($id)){
            output_error('参数错误');
        }
        $data = $article->getOneArticle($id);
        $data['article_content'] = htmlspecialchars($data['article_content']);
        output_data($data);
	}

}
