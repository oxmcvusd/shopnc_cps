<?php
/**
 * 手机接口初始化文件
 *
 *
 * @copyright  Copyright (c) 2007-2013 BesonIT Inc. (http://www.besonit.com)
 * @license    http://www.besonit.com
 * @link       http://www.besonit.com
 * @since      File available since Release v1.1
 */

define('APP_ID','weixin');
define('ENV','dev');//dev开发模式，pdt生产模式
define('IGNORE_EXCEPTION', true);
define('BASE_PATH',str_replace('\\','/',dirname(__FILE__)));

if (!@include(dirname(dirname(__FILE__)).'/global.php')) exit('global.php isn\'t exists!');
if (!@include(BASE_CORE_PATH.'/shopjl.php')) exit('shopnc.php isn\'t exists!');

if (!@include(BASE_PATH.'/config/config.ini.php')){
    exit('config.ini.php isn\'t exists!');
}

define('SITE_URL',$config['site_url']);
define('API_PATH',BASE_PATH.'/api');
//框架扩展
require(BASE_PATH.'/framework/function/function.php');
if (!@include(BASE_PATH.'/control/control.php')) exit('control.php isn\'t exists!');
require(BASE_PATH.'/framework/libraries/wxSdk.class.php');
define('APP_SITE_URL',SITE_URL.'/weixin');
define('WX_SITE_URL',APP_SITE_URL);
define('TPL_NAME',TPL_SHOP_NAME);
define('SHOP_RESOURCE_SITE_URL',APP_SITE_URL.DS.'resource');
define('SHOP_TEMPLATES_URL',APP_SITE_URL.'/templates/'.TPL_NAME);
define('BASE_TPL_PATH',BASE_PATH.'/templates/'.TPL_NAME);
define('BASE_ORDER_LOCK_PATH',BASE_PATH.'/data/lock/');
@Base::run();