<?php
defined('BYshopJL') or exit('Access Invalid!');
/**
 * Description:
 * Author: walkskyer
 * Email:zwj_work@qq.com
 * Date: 2015/4/13
 * Time: 16:18
 */
class wx_bs_profit_logModel extends Model
{
    public function __construct($table = null)
    {
        parent::__construct('bs_profit_log');
    }

    public function getList($condition, $order='bs_profit_log.dateline desc'){
        $data_list = $this->table('bs_profit_log,member')
            ->field('bs_profit_log.*')
            ->join('left')->on('bs_profit_log.member_id = member.member_id')
            ->where($condition)->order($order)->select();
        if (empty($data_list)) return array();
        return $data_list;
    }
}