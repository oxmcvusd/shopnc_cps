<?php
/**
 * Created by PhpStorm.
 * User: BesonTD
 * Date: 2015/3/28
 * Time: 9:53
 */
defined('BYshopJL') or exit('Access Invalid!');
//require_once(BASE_DATA_PATH.'/model/model.model.php');
class wx_bs_brokerageModel extends Model{
    public function __construct(){
        parent::__construct('bs_brokerage');
        $this->pk='bk_id';
    }
    public function getBrokerage($condition){
        $data = $this->where($condition)->find();
        if (empty($data)) return array();
        return $data;
    }

    public function getProfitList($condition, $order='brokerage_id desc'){
        $data_list = $this->table('bs_profit_log,order')
            ->field('bs_profit_log.*,order.order_sn,order.finnshed_time')
            ->join('left')->on('bs_profit_log.order_id = order.order_id')
            ->where($condition)->order($order)->select();
        if (empty($data_list)) return array();
        return $data_list;
    }

    public function save($data,$condition){
        if(isset($condition['bk_member_id']) && !empty($condition['bk_member_id']) && !$this->where('bk_member_id='.$condition['bk_member_id'])->select()){
            $this->insert(array('bk_member_id'=>$condition['bk_member_id']));
        }
        return $this->update($data,array('where'=>$condition));
    }
}