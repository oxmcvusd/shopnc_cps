<?php
/**
 * 商品类别模型
 *
 *
 *
 *
 * by www.besonit.com
 */

defined('BYshopJL') or exit('Access Invalid!');

require_once(BASE_DATA_PATH.'/model/goods_class.model.php');
class wx_goods_classModel extends goods_classModel
{
    /**
     * 取得店铺绑定的分类
     *
     * @param   number  $store_id   店铺id
     * @param   number  $pid        父级分类id
     * @param   number  $deep       深度
     * @return  array   二维数组
     */
    public function getGoodsClass($store_id, $pid = 0, $deep = 1) {
        // 读取商品分类
        $gc_list = $this->getGoodsClassListByParentId($pid);
            $gc_list = array_under_reset($gc_list, 'gc_id');
            $model_storebindclass = Model('store_bind_class');
            $gcid_array = $model_storebindclass->getStoreBindClassList(array(
                'store_id' => $store_id,
                'state' => array('in', array(1, 2)),
            ), '', "class_{$deep} asc", "distinct class_{$deep}");

            if (!empty($gcid_array)) {
                $tmp_gc_list = array();
                foreach ($gcid_array as $value) {
                    if (isset($gc_list[$value["class_{$deep}"]])) {
                        $tmp_gc_list[] = $gc_list[$value["class_{$deep}"]];
                    }
                }
                $gc_list = $tmp_gc_list;
            } else {
                return array();
            }
        return $gc_list;
    }
}
