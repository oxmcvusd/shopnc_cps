<?php
/**
 * Created by PhpStorm.
 * User: BesonTD
 * Date: 2015/3/28
 * Time: 9:53
 */
defined('BYshopJL') or exit('Access Invalid!');
//require_once(BASE_DATA_PATH.'/model/model.model.php');
class wx_profit_logModel extends Model{
    public function __construct(){
        parent::__construct('bs_profit_log');
    }

    public function getProfitList($condition, $order='profit_log_id desc'){
        $data_list = $this->table('bs_profit_log,order')
            ->field('bs_profit_log.*,order.order_sn,order.finnshed_time')
            ->join('left')->on('bs_profit_log.order_id = order.order_id')
            ->where($condition)->order($order)->select();
        if (empty($data_list)) return array();
        return $data_list;
    }

}