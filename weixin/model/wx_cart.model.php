<?php
/**
 * Created by PhpStorm.
 * User: BesonTD
 * Date: 2015/3/28
 * Time: 9:53
 */
defined('BYshopJL') or exit('Access Invalid!');
require_once(BASE_DATA_PATH.'/model/cart.model.php');
class wx_cartModel extends cartModel{

    /**
     * 购物车商品总金额
     */
    private $cart_all_price = 0;

    /**
     * 购物车商品总数
     */
    private $cart_goods_num = 0;

    public function __construct(){
        parent::__construct();
    }


    /**
     * 购物车列表
     *
     * @param string $type 存储类型 db,cookie
     * @param unknown_type $condition
     * @param int $limit
     */
    public function listCart($type, $condition = array(), $limit = '') {
        if ($type == 'db') {
            $cart_list = $this->where($condition)->limit($limit)->select();
        } elseif ($type == 'cookie') {
            //去除斜杠
            $cart_str = get_magic_quotes_gpc() ? stripslashes(cookie('cart')) : cookie('cart');
            $cart_str = base64_decode(decrypt($cart_str));
            $cart_list = @unserialize($cart_str);
        }
        $cart_list = is_array($cart_list) ? $cart_list : array();
        //顺便设置购物车商品数和总金额
        $this->cart_goods_num =  count($cart_list);
        $cart_all_price = 0;
        if(is_array($cart_list)) {
            foreach ($cart_list as $val) {
                $cart_all_price	+= ($val['goods_price']-$val['profit']) * $val['goods_num'];
            }
        }
        $this->cart_all_price = ncPriceFormat($cart_all_price);
        return !is_array($cart_list) ? array() : $cart_list;
    }


    /**
     * 将商品添加到购物车中
     *
     * @param array	$data	商品数据信息
     * @param string $save_type 保存类型，可选值 db,cookie
     * @param int $quantity 购物数量
     */
    public function addCart($data = array(), $save_type = '', $quantity = null) {
        $method = '_addCart'.ucfirst($save_type);
        $insert = $this->$method($data,$quantity);
        //更改购物车总商品数和总金额，传递数组参数只是给DB使用
        $this->getCartNum($save_type,array('buyer_id'=>$data['buyer_id']));
        return $insert;
    }

    /**
     * 添加数据库购物车
     *
     * @param unknown_type $goods_info
     * @param unknown_type $quantity
     * @return unknown
     */
    private function _addCartDb($goods_info = array(),$quantity) {
        //验证购物车商品是否已经存在
        $condition = array();
        $condition['goods_id'] = $goods_info['goods_id'];
        $condition['buyer_id'] = $goods_info['buyer_id'];
        if (isset($goods_info['bl_id'])) {
            $condition['bl_id'] = $goods_info['bl_id'];
        } else {
            $condition['bl_id'] = 0;
        }
        $check_cart	= $this->checkCart($condition);
        if (!empty($check_cart)) return true;

        $array    = array();
        $array['buyer_id']	= $goods_info['buyer_id'];
        $array['store_id']	= $goods_info['store_id'];
        $array['goods_id']	= $goods_info['goods_id'];
        $array['goods_name'] = $goods_info['goods_name'];
        $array['goods_price'] = $goods_info['goods_price'];
        $array['goods_num']   = $quantity;
        $array['goods_image'] = $goods_info['goods_image'];
        $array['store_name'] = $goods_info['store_name'];
        $array['goods_profit'] = $goods_info['goods_profit'];
        $array['goods_brokerage'] = $goods_info['goods_brokerage'];
        $array['bl_id'] = isset($goods_info['bl_id']) ? $goods_info['bl_id'] : 0;
        return $this->insert($array);
    }

    /**
     * 添加到cookie购物车,最多保存5个商品
     *
     * @param unknown_type $goods_info
     * @param unknown_type $quantity
     * @return unknown
     */
    private function _addCartCookie($goods_info = array(), $quantity = null) {
        //去除斜杠
        $cart_str = get_magic_quotes_gpc() ? stripslashes(cookie('cart')) : cookie('cart');
        $cart_str = base64_decode(decrypt($cart_str));
        $cart_array = @unserialize($cart_str);
        $cart_array = !is_array($cart_array) ? array() : $cart_array;
        if (count($cart_array) >= 5) return false;

        if (in_array($goods_info['goods_id'],array_keys($cart_array))) return true;
        $cart_array[$goods_info['goods_id']] = array(
            'store_id' => $goods_info['store_id'],
            'goods_id' => $goods_info['goods_id'],
            'goods_name' => $goods_info['goods_name'],
            'goods_price' => $goods_info['goods_price'],
            'goods_image' => $goods_info['goods_image'],
            'goods_profit' => $goods_info['goods_profit'],
            'goods_brokerage' => $goods_info['goods_brokerage'],
            'goods_num' => $quantity
        );
        setNcCookie('cart',encrypt(base64_encode(serialize($cart_array))),24*3600);
        return true;
    }

    /**
     * 计算购物车总商品数和总金额
     * @param string $type 购物车信息保存类型 db,cookie
     * @param array $condition 只有登录后操作购物车表时才会用到该参数
     */
    public function getCartNum($type, $condition = array()) {
        if ($type == 'db') {
            $cart_all_price = 0;
            $cart_goods	= $this->listCart('db',$condition);
            $this->cart_goods_num = count($cart_goods);
            if(!empty($cart_goods) && is_array($cart_goods)) {
                foreach ($cart_goods as $val) {
                    $cart_all_price	+= $val['goods_price'] * $val['goods_num'];
                }
            }
            $this->cart_all_price = ncPriceFormat($cart_all_price);
        } elseif ($type == 'cookie') {
            $cart_str = get_magic_quotes_gpc() ? stripslashes(cookie('cart')) : cookie('cart');
            $cart_str = base64_decode(decrypt($cart_str));
            $cart_array = @unserialize($cart_str);
            $cart_array = !is_array($cart_array) ? array() : $cart_array;
            $this->cart_goods_num = count($cart_array);
            $cart_all_price = 0;
            foreach ($cart_array as $v){
                $cart_all_price += floatval($v['goods_price'])*intval($v['goods_num']);
            }
            $this->cart_all_price = $cart_all_price;
        }
        @setNcCookie('cart_goods_num',$this->cart_goods_num,2*3600);
        return $this->cart_goods_num;
    }


}