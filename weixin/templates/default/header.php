<?php defined('BYshopJL') or exit('Access Invalid!');?>
<html lang="en"><head>
    <meta charset="UTF-8">
    <title>商品列表</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <link href="<?php echo SHOP_TEMPLATES_URL; ?>/css/reset.css" type="text/css" rel="stylesheet">
    <link href="<?php echo SHOP_TEMPLATES_URL; ?>/css/main.css" type="text/css" rel="stylesheet">
</head>
