<?php defined('BYshopJL') or exit('Access Invalid!');?>
<header id="header">
    <div class="header-wrap">
        <a class="header-back" href="javascript:history.back();"><span>返回</span></a>
        <h2>商品列表</h2>
        <a class="i-main-opera" id="btn-opera" href="javascript:void(0)"><span></span></a>
    </div>
    <div class="main-opera-pannel">
        <div class="main-op-table main-op-warp">
            <a class="quarter" href="/wap/index.html"><span class="i-home"></span><p>首页</p></a>
            <a class="quarter" href="/wap/tmpl/product_first_categroy.html"><span class="i-categroy"></span><p>分类</p></a>
            <a class="quarter" href="/wap/tmpl/cart_list.html"><span class="i-cart"></span><p>购物车</p></a>
            <a class="quarter" href="/wap/tmpl/member/member.html?act=member"><span class="i-mine"></span><p>我的商城</p></a>
        </div>
    </div>
</header>
