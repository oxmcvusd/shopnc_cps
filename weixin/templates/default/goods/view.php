<?php
/**
 * Description:
 * Author: walkskyer
 * Email:zwj_work@qq.com
 * Date: 2015/3/30
 * Time: 16:23
 */
extract($output['goods_detail']);
?>
<body>
<link href="./templates/default/css/child.css" type="text/css" rel="stylesheet">
<header id="header">
    <div class="header-wrap"><a class="header-back" href="javascript:history.back();"><span>返回</span></a>

        <h2>商品详情</h2><a class="i-main-opera" id="btn-opera" href="javascript:void(0)"><span></span></a></div>
    <div class="main-opera-pannel">
        <div class="main-op-table main-op-warp"><a class="quarter" href="/wap/index.html"><span class="i-home"></span>

                <p>首页</p></a><a class="quarter" href="/wap/tmpl/product_first_categroy.html"><span
                    class="i-categroy"></span>

                <p>分类</p></a><a class="quarter" href="/wap/tmpl/cart_list.html"><span class="i-cart"></span>

                <p>购物车</p></a><a class="quarter" href="/wap/tmpl/member/member.html?act=member"><span
                    class="i-mine"></span>

                <p>我的商城</p></a></div>
    </div>
</header>
<div id="product_detail_wp">
    <div class="content">
        <div class="pddetail-cnt">
            <div class="pddc-topwp">
                <a class="pddct-imgwp" href="javascript:void(0);">
                    <div class="swiper-container" id="mySwipe" style="visibility: visible;">
                        <div class="swipe-wrap" style="width: 1280px;">
                            <?
                            $images=explode(',',$goods_image);
                            $images_count=count($images);
                            foreach($images as $v){?>
                                <div class="swipe-item"
                                     style="width: 320px; left: 0px; transition-duration: 0ms; transform: translateX(0px);"
                                     data-index="0"><img src="<?=$v?>">
                                </div>
                            <? }
                            ?>
                        </div>
                    </div>
                    <div class="pddct-shadow"></div>
                    <div class="pddct-name-wp">
                        <div class="pddctnw-name">
                            <?=$goods_info['goods_name']?>
                        </div>
                    </div>
                    <span class="pd-collect">收藏</span>
                    <span class="pdpic-size-bg"></span>
						<span class="pdpic-size">
							<span class="pds-cursize">1</span>
							/
							<span class="pds-tsize"><?=$images_count?></span>
						</span>
                </a>
            </div>
            <div class="pddc-gray-warp">
                <a class="pddetail-go-title clearfix" href="product_info.html?goods_id=51">
	                    <span class="pgt-title fleft">
	                        图文详情
	                    </span>
	                    <span class="pgt-go fright">
	                        <span class="i-go-right"></span>
	                    </span>
                </a>
            </div>
            <div class="pddc-property-one pddc-gray-warp">
                <div class="pddcp-one-wp ppdc-white-wrap">
                    <div class="pddcp-one-top">
                        <ul>


                            <li class="clearfix">
                                <span class="key">价格：</span>

                                <div class="price value">
                                    ￥<?=$goods_info['goods_price']?>
                                </div>
                            </li>

                            <li class="clearfix">
                                <span class="key">市场价：</span>

                                <div class="value">
                                    <del>￥<?=$goods_info['goods_marketprice']?></del>
                                </div>
                            </li>
                            <li class="clearfix">
                                <span class="key">销量：</span>

                                <div class="value"><?=$goods_info['goods_salenum']?>件</div>
                            </li>


                        </ul>
                        <div class="pddcp-arrow">
                            <span class="graydownarrow"></span>
                        </div>
                    </div>
                    <div class="pddcp-one-hide">
                        <div class="clearfix">
                            <span class="key">商品描述：</span>
                        </div>
                        <p id="mobile_body">
                            <?=$goods_info['goods_name']?$goods_info['goods_name']:'暂无商品描述'?>暂无商品描述
                        </p>
                    </div>
                </div>
            </div>

            <div class="pddc-gray-warp">
                <ul class="pddc-active ppdc-white-wrap">
                    <li class="clearfix">
                        <span class="key">活动名称：</span>

                        <div class="value">

                            暂无

                        </div>
                    </li>
                    <li class="bd-tdashed-dd">
                        <span class="no-key">活动描述：</span>

                        暂无

                    </li>
                </ul>
            </div>
            <div class="pddc-gray-warp">
                <ul class="pddc-stock ppdc-white-wrap">
                    <li class="pddc-stock-title clearfix">
                        <span class="key">库存：</span>

                        <div class="price value">
                            <span class="stock-num"><?=$goods_info['goods_storage']?></span>
                            件
                        </div>
                    </li>


                    <li class="pddc-stock-spec bd-tdashed-dd">
                        <?
                        foreach($goods_info['spec_name'] as $k=>$name){?>
                            <span spec_id="" class="key-no">
								<?=$name?>：
							</span>

                            <div class="value-no mt10">
                                <? foreach($goods_info['spec_value'][$k] as $s_id=>$s_name){?>

                                    <a specs_value_id="<?=$s_id?>" <?if($k==1 && $s_id==$goods_info['color_id']){?> class="current" <? }?> href="javascript:void(0);">
                                        <?=$s_name?>
                                        <i class="pd-choice-icon"></i>
                                    </a>
                                <? }?>
                            </div>
                        <? }?>
                    </li>


                    <li class="bd-tdashed-dd">
							<span class="key-no">
								数量：
							</span>

                        <div class="value-no mt10 clearfix">
								<span class="minus-wp fleft">
										<span class="i-minus"></span>
								</span>
                            <input type="text" value="1" id="buynum" class="buy-num fleft">
								<span class="add-wp fleft">
									<span class="i-add"></span>
								</span>
                        </div>
                    </li>
                    <li class="bd-tdashed-dd">
                        <div class="opera-product-wp">

                            <div class="opera-pd-item buy-now">立即购买</div>

                            <div class="opera-pd-item add-to-cart">加入购物车</div>


                        </div>
                    </li>
                </ul>
            </div>
            <div class="pddc-gray-warp">
                <div class="pddc-commend-list">
                    <span class="pddc-commendl-title">商品推荐：</span>

                    <div class="pddc-commend-wp clearfix">
                        <? foreach($goods_commend_list as $k=>$v){?>
                        <a href="?act=goods&op=goods_detail&goods_id=<?=$v['goods_id']?>">
                            <img src="<?=$v['goods_image_url']?>">
									<span title="<?=$v['goods_name']?>" class="pddc-commendw-t">
										<?=$v['goods_name']?>
									</span>
                            <span title="￥<?=$v['goods_price']?>" class="pddc-commendw-price">￥<?=$v['goods_price']?></span>
                        </a>
                        <? }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var goods_id=<?=$goods_info['goods_id']?$goods_info['goods_id']:0?>;
    var data=<?=json_encode($output['goods_detail'])?>
</script>

<footer id="footer">
    <div class="footer">
        <div class="footer-top">
            <div class="footer-tleft"><a href="/wap/tmpl/member/member.html?act=member" class="btn mr5">个人中心</a></div>
            <a class="gotop" href="javascript:void(0);"><span class="gotop-icon"></span>

                <p>回顶部</p></a></div>
        <div class="footer-content"><p class="link"><a class="standard" href="/">标准版</a><a
                    href="/download/app/AndroidShopNC2014Moblie.apk">下载Android客户端</a></p>

            <p class="copyright">版权所有 2013-2014 &copy; 网店交流</p></div>
    </div>
</footer>

<script src="./templates/default/js/zepto.min.js" type="text/javascript"></script>
<script src="./templates/default/js/config.js" type="text/javascript"></script>
<!--<script src="./templates/default/js/template.js" type="text/javascript"></script>-->
<script src="./templates/default/js/swipe.js" type="text/javascript"></script>
<script src="./templates/default/js/common.js" type="text/javascript"></script>
<script src="./templates/default/js/simple-plugin.js" type="text/javascript"></script>
<!--<script src="./templates/default/js/tmpl/common-top.js" type="text/javascript"></script>-->
<script src="./templates/default/js/tmpl/footer.js" type="text/javascript"></script>
<!--<script src="./templates/default/js/tmpl/product_detail.js" type="text/javascript"></script>-->
<script type="text/javascript">
    //加入购物车
    $(".add-to-cart").click(function (){
        var key = getcookie('key');//登录标记
        if(key==''){
            window.location.href = WapSiteUrl+LoginUrl;
        }else{
            var quantity = parseInt($(".buy-num").val());
            $.ajax({
                url:ApiUrl+"/index.php?act=member_cart&op=cart_add",
                data:{key:key,goods_id:goods_id,quantity:quantity},
                type:"post",
                success:function (result){
                    var rData = $.parseJSON(result);
                    if(checklogin(rData.login)){
                        if(!rData.datas.error){
                            $.sDialog({
                                skin:"block",
                                content:"添加购物车成功！",
                                "okBtnText": "再逛逛",
                                "cancelBtnText": "去购物车",
                                okFn:function (){},
                                cancelFn:function (){
                                    window.location.href = WapSiteUrl+'/tmpl/cart_list.html';
                                }
                            });
                        }else{
                            $.sDialog({
                                skin:"red",
                                content:rData.datas.error,
                                okBtn:false,
                                cancelBtn:false
                            });
                        }
                    }
                }
            })
        }
    });

    //购买数量，减
    $(".minus-wp").click(function (){
        var buynum = $(".buy-num").val();
        if(buynum >1){
            $(".buy-num").val(parseInt(buynum-1));
        }
    });
    //购买数量加
    $(".add-wp").click(function (){
        var buynum = parseInt($(".buy-num").val());
        if(buynum < data.goods_info.goods_storage){
            $(".buy-num").val(parseInt(buynum+1));
        }
    });

    // 图片轮播
    function picSwipe(){
        var elem = $("#mySwipe")[0];
        window.mySwipe = Swipe(elem, {
            continuous: true,
            // disableScroll: true,
            stopPropagation: true,
            callback: function(index, element) {
                $(".pds-cursize").html(index+1);
            }
        });
    }
    picSwipe();
</script>