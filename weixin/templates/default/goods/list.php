<link href="<?php echo SHOP_TEMPLATES_URL; ?>/css/child.css" type="text/css" rel="stylesheet">
<body id="home-320">
        <?php require_once template('common_header');?>
		<div class="content">
		<div class="product-filter">
			<a key="4" class="clearfix current keyorder" href="javascript:void(0);">
				<span class="pf-newpd-icon f-icon fleft"></span>
				<span class="pf-title">新品</span>
			</a>
			<a key="3" class="clearfix keyorder" href="javascript:;">
				<span class="pf-price-icon  desc f-icon fleft"></span>
				<span class="pf-title">价格</span>
			</a>
			<a key="1" class="clearfix keyorder" href="javascript:;">
				<span class="pf-sales-icon f-icon fleft"></span>
				<span class="pf-title">销量</span>
			</a>
			<a key="2" class="clearfix keyorder" href="javascript:;">
				<span class="pf-popularity-icon f-icon fleft"></span>
				<span class="pf-title">人气</span>
			</a>
		</div>
		<div class="product-cnt">
			<div id="product_list">
		<ul class="product-list">
<?php
        if($output['goods_list']){
            foreach($output['goods_list'] as $v){


?>
               <li goods_id="'.$v['goods_id'].'" class="pdlist-item">
				<a class="pdlist-item-wrap clearfix" href="<?php echo urlWx('goods','goods_detail',array('goods_id'=>$v['goods_id'])); ?>">
					<span class="pdlist-iw-imgwp">
						<img src="<?php echo $v['goods_image_url'];?>">
					</span>
					<div class="pdlist-iw-cnt">
						<p class="pdlist-iwc-pdname"><?php echo $v['goods_name']; ?></p>
						<p class="pdlist-iwc-pdprice">￥<?php echo $v['goods_price']; ?>
                        <?php
                            if ($v['is_virtual'] == '1') {?>
                            <span class="product-status bg-virtual">虚拟兑换</span>
                        <?php    } else { ?>
                        <?php   if ($v['is_presell'] == '1') { ?>
                            <span class="product-status bg-presell">预售</span>
                     <?php        }
                            if ($v['is_fcode']== '1') {?>
                            <span class="product-status bg-fcode">F码优先</span>
                            <?php
                            }
                            }
                        ?>

						</p>
						<p class="pdlist-iwc-pdcomment  clearfix">
							<span style="margin-top:-5px;" class="evaluation_good_swp mr5 fleft">
                                <?php
                                for($s = 1;$s<=5;$s++){
                                    if ($s<=$v['evaluation_good_star']) {
                                        echo '<span class="evaluation-star fleft"></span>';
                                    }else{
                                        echo '<span class="evaluation-star-gray fleft"></span>';
                                    }
                                }

                                ?>
							</span>
							<span class="fleft">
                                (<?php echo $v['evaluation_count'];?>人)
							</span>
						</p>
					</div>
				</a>
			</li>
        <?php

            }
        }else{
            echo '<div class="no-record">暂无记录</div>';
        }

        ?>


		</ul>
	</div>
			<div class="pagination mt10">
				<a class="pre-page " href="javascript:void(0);">上一页</a>
				<select style="padding: 7px 4px;  vertical-align: top;" name="page_list"><option selected="" value="1">1</option></select>
				<a class="next-page  " href="javascript:void(0);">下一页</a>
			</div>
		</div>
	</div>

    <input type="hidden" value="<?php echo $output['key'];?>" name="key">
    <input type="hidden" value="<?php echo $output['order'];?>" name="order">
    <input type="hidden" value="10" name="page">
    <input type="hidden" value="<?php echo $output['curpage'];?>" name="curpage">
    <input type="hidden" name="hasmore" value="<?php echo $output['pageSet']['hasmore'];?>">
    <input type="hidden" value="<?php ?>" name="gc_id">
    <input type="hidden" name="keyword" value="<?php if($output['keyword']){echo $output['keyword'];}?>">


    <?php require_once template('common_footer');?>
<script>
    $(function(){
        var hasmore = $("input[name=hasmore]").val();
        if(!hasmore){
            $('.next-page').addClass('disabled');
        }else{
            $('.next-page').removeClass('disabled');
        }

        var curpage = $("input[name=curpage]").val();//分页
        var page_total = <?php echo $output['pageSet']['page_total'];?>;
        var page_html = '';
        if(curpage==1){
            $('.pre-page').addClass('disabled');
        }else{
            $('.pre-page').removeClass('disabled');
        }
        for(var i=1;i<=page_total;i++){
            if(i==curpage){
                page_html+='<option value="'+i+'" selected>'+i+'</option>';
            }else{
                page_html+='<option value="'+i+'">'+i+'</option>';
            }
        }

        $('select[name=page_list]').empty();
        $('select[name=page_list]').append(page_html);








//        $("input[name=keyword]").val(escape(GetQueryString('keyword')));
//        $("input[name=gc_id]").val(GetQueryString('gc_id'));


        $(".page-warp").click(function (){
            $(this).find(".pagew-size").toggle();
        });

        if($("input[name=gc_id]").val()!=''){

        }else{

        }


        $("select[name=page_list]").change(function(){
            var key = parseInt($("input[name=key]").val());
            var order = parseInt($("input[name=order]").val());
            var page = parseInt($("input[name=page]").val());
            var gc_id = parseInt($("input[name=gc_id]").val());
            var keyword = $("input[name=keyword]").val();
            var hasmore = $("input[name=hasmore]").val();

            var curpage = $('select[name=page_list]').val();

            if(gc_id>0){
                var url = ApiUrl+"/index.php?act=goods&op=goods_list&key="+key+"&order="+order+"&page="+page+"&curpage="+curpage+"&gc_id="+gc_id;
            }else{
                var url = ApiUrl+"/index.php?act=goods&op=goods_list&key="+key+"&order="+order+"&page="+page+"&curpage="+curpage+"&keyword="+keyword;
            }

            window.location.href = url;

        });


        $('.keyorder').click(function(){
            var key = parseInt($("input[name=key]").val());
            var order = parseInt($("input[name=order]").val());
            var page = parseInt($("input[name=page]").val());
            var curpage = eval(parseInt($("input[name=curpage]").val())-1);
            var gc_id = parseInt($("input[name=gc_id]").val());
            var keyword = $("input[name=keyword]").val();
            var hasmore = $("input[name=hasmore]").val();

            var curkey = $(this).attr('key');//1.销量 2.浏览量 3.价格 4.最新排序
            if(curkey == key){
                if(order == 1){
                    var curorder = 2;
                }else{
                    var curorder = 1;
                }
            }else{
                var curorder = 1;
            }

            if (curkey == 3) {
                if (curorder == 1) {
                    $(this).find('span').removeClass('desc').addClass('asc');
                } else {
                    $(this).find('span').removeClass('asc').addClass('desc');
                }
            }

            $(this).addClass("current").siblings().removeClass("current");

            if(gc_id>0){
                var url = ApiUrl+"/index.php?act=goods&op=goods_list&key="+curkey+"&order="+curorder+"&page="+page+"&curpage=1&gc_id="+gc_id;
            }else{
                var url = ApiUrl+"/index.php?act=goods&op=goods_list&key="+curkey+"&order="+curorder+"&page="+page+"&curpage=1&keyword="+keyword;
            }
            window.location.href = url;
        });

        $('.pre-page').click(function(){//上一页
            var key = parseInt($("input[name=key]").val());
            var order = parseInt($("input[name=order]").val());
            var page = parseInt($("input[name=page]").val());
            var curpage = eval(parseInt($("input[name=curpage]").val())-1);
            var gc_id = parseInt($("input[name=gc_id]").val());
            var keyword = $("input[name=keyword]").val();

            if(curpage<1){
                return false;
            }

            if(gc_id>=0){
                var url = ApiUrl+"/index.php?act=goods&op=goods_list&key="+key+"&order="+order+"&page="+page+"&curpage="+curpage+"&gc_id="+gc_id;
            }else{
                var url = ApiUrl+"/index.php?act=goods&op=goods_list&key="+key+"&order="+order+"&page="+page+"&curpage="+curpage+"&keyword="+keyword;
            }
            window.location.href = url;
        });

        $('.next-page').click(function(){//下一页
            var hasmore = $('input[name=hasmore]').val();
            if(hasmore == 'false'){
                return false;
            }

            var key = parseInt($("input[name=key]").val());
            var order = parseInt($("input[name=order]").val());
            var page = parseInt($("input[name=page]").val());
            var curpage = eval(parseInt($("input[name=curpage]").val())+1);
            var gc_id = parseInt($("input[name=gc_id]").val());
            var keyword = $("input[name=keyword]").val();

            if(gc_id>=0){
                var url = ApiUrl+"/index.php?act=goods&op=goods_list&key="+key+"&order="+order+"&page="+page+"&curpage="+curpage+"&gc_id="+gc_id;
            }else{
                var url = ApiUrl+"/index.php?act=goods&op=goods_list&key="+key+"&order="+order+"&page="+page+"&curpage="+curpage+"&keyword="+keyword;
            }
            window.location.href = url;
        });

    });
</script>