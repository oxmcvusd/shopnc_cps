<link href="<?php echo SHOP_TEMPLATES_URL; ?>/css/member.css" type="text/css" rel="stylesheet">
<body>
<?php require_once template('common_header'); ?>
<div class="address-opera">
    <div class="address-ocnt">
        <div class="address-octlt">收货人信息</div>
        <p>姓名：<span class="opera-tips">(*必填)</span></p>

        <p>
            <input type="text" name="true_name" id="true_name" class="input-30"
                   value="<?= $output['address_info']['true_name']; ?>">
        </p>

        <p>手机号码：<span class="opera-tips">(*必填)</span></p>

        <p>
            <input type="text" name="mob_phone" id="mob_phone" class="input-30"
                   value="<?= $output['address_info']['mob_phone']; ?>">
        </p>

        <p>电话号码：</p>

        <p>
            <input type="text" name="tel_phone" class="input-30" value="<?= $output['address_info']['tel_phone']; ?>">
        </p>

        <div class="address-octlt">地址信息</div>
        <?php if (empty($output['address_add'])) {?>
           <p><span class="mr5" id="detailaddr"><?= $output['address_info']['area_info']; ?></span></p>
        <p class="detail-d-addr"><?= $output['address_info']['address']; ?></p>
        <?php }?>
        <p><a id="editaddress" class="btn-s btn-prink-s" href="javascript:;" <?php if ($output['address_add'] == 1) { ?> style="display:none;"<?php } ?>>编辑</a><input type="hidden" value="1" name="modifyaddr"/></p>
        <div id="area" <?php if ($output['address_add'] != 1) { ?> style="display:none;"<?php } ?>>
            <p>省份：<span class="opera-tips">(*必填)</span></p>

            <div class="new-select-wp" id="prov">
                <select class="select-30" id="prov_select" name="prov">
                    <option value="">请选择...</option>
                </select>
            </div>
            <p>城市：<span class="opera-tips">(*必填)</span></p>

            <div class="new-select-wp" id="city">
                <select class="select-30" id="city_select" name="city">
                    <option value="">请选择...</option>
                </select>
            </div>
            <p>区县：<span class="opera-tips">(*必填)</span></p>

            <div class="new-select-wp" id="region">
                <select class="select-30" id="region_select" name="region">
                    <option value="">请选择...</option>
                </select>
            </div>
            <p>街道：<span class="opera-tips">(*必填)</span></p>

            <p>
                <input type="text" class="input-30" id="address" name="address">
            </p>
        </div>
    </div>
</div>
<input type="hidden" name="area_id" value="<?= $output['address_info']['area_id']; ?>">
<input type="hidden" name="city_id" value="<?= $output['address_info']['city_id']; ?>">
<input type="hidden" name="area_info" value="<?= $output['address_info']['area_info']; ?>">
<input type="hidden" name="address">

<div class="error-tips"></div>
<a href="javascript:;" class="add_address mt10">保存地址</a>

<?php require_once template('common_footer'); ?>
<script src="<?php echo SHOP_TEMPLATES_URL; ?>/js/simple-plugin.js" type="text/javascript"></script>
<script>
    $(function () {
        var address_id = <?php if($output['address_info']['address_id']){echo $output['address_info']['address_id'];}else{echo 0;}?>;
        $('#editaddress').click(function () {
            if ($('input[name=modifyaddr]').val() == '1') {
                $('input[name=modifyaddr]').val(2);
                $('#area').show();
            } else {
                $('input[name=modifyaddr]').val(1);
                $('#area').hide();
            }
        });
        $.ajax({
            type: 'post',
            url: ApiUrl + '/index.php?act=member_address&op=area_list',
            dataType: 'json',
            success: function (result) {
                var data = result.datas;
                var prov_html = '';
                for (var i = 0; i < data.area_list.length; i++) {
                    prov_html += '<option value="' + data.area_list[i].area_id + '">' + data.area_list[i].area_name + '</option>';
                }
                $("select[name=prov]").append(prov_html);
            }
        });
        $("select[name=prov]").change(function () {
            var prov_id = $(this).val();
            $.ajax({
                type: 'post',
                url: ApiUrl + '/index.php?act=member_address&op=area_list',
                data: {
                    area_id: prov_id
                },
                dataType: 'json',
                success: function (result) {
                    var data = result.datas;
                    var city_html = '<option value="">请选择...</option>';
                    for (var i = 0; i < data.area_list.length; i++) {
                        city_html += '<option value="' + data.area_list[i].area_id + '">' + data.area_list[i].area_name + '</option>';
                    }
                    $("select[name=city]").html(city_html);
                    $("select[name=region]").html('<option value="">请选择...</option>');
                }
            });
        });

        $("select[name=city]").change(function () {
            var city_id = $(this).val();
            $.ajax({
                type: 'post',
                url: ApiUrl + '/index.php?act=member_address&op=area_list',
                data: {
                    area_id: city_id
                },
                dataType: 'json',
                success: function (result) {
                    var data = result.datas;
                    var region_html = '<option value="">请选择...</option>';
                    for (var i = 0; i < data.area_list.length; i++) {
                        region_html += '<option value="' + data.area_list[i].area_id + '">' + data.area_list[i].area_name + '</option>';
                    }
                    $("select[name=region]").html(region_html);
                }
            });
        });


        $.sValid.init({
            rules: {
                true_name: "required",
                mob_phone: "required",
                prov_select: "required",
                city_select: "required",
                region_select: "required",
                address: "required"
            },
            messages: {
                true_name: "姓名必填！",
                mob_phone: "手机号必填！",
                prov_select: "省份必填！",
                city_select: "城市必填！",
                region_select: "区县必填！",
                address: "街道必填！"
            },
            callback: function (eId, eMsg, eRules) {
                if (eId.length > 0) {
                    var errorHtml = "";
                    $.map(eMsg, function (idx, item) {
                        errorHtml += "<p>" + idx + "</p>";
                    });
                    $(".error-tips").html(errorHtml).show();
                } else {
                    $(".error-tips").html("").hide();
                }
            }
        });
        $('.add_address').click(function () {
            var postUrl;
            if (address_id !== 0) {
                postUrl = ApiUrl + "/index.php?act=member_address&op=address_edit";
            } else {
                $('input[name=modifyaddr]').val(2);
                postUrl = ApiUrl + "/index.php?act=member_address&op=address_add";
            }
            if ($.sValid()) {
                var true_name = $('input[name=true_name]').val();
                var mob_phone = $('input[name=mob_phone]').val();
                var tel_phone = $('input[name=tel_phone]').val();

                var op = $('input[name=modifyaddr]').val();
                if (op == '2') {
                    var city_id = $('select[name=city]').val();
                    var area_id = $('select[name=region]').val();
                    var address = $('input[name=address]').val();

                    var prov_index = $('select[name=prov]')[0].selectedIndex;
                    var city_index = $('select[name=city]')[0].selectedIndex;
                    var region_index = $('select[name=region]')[0].selectedIndex;
                    var area_info = $('select[name=prov]')[0].options[prov_index].innerHTML + ' ' + $('select[name=city]')[0].options[city_index].innerHTML + ' ' + $('select[name=region]')[0].options[region_index].innerHTML;


                } else {
                    var city_id = $('input[name=city_id]').val();
                    var area_id = $('input[name=area_id]').val();
                    var address = $('input[name=address]').val();
                    var area_info = $('input[name=area_info]').val();
                }


                $.ajax({
                    type: 'post',
                    url: postUrl,
                    data: {
                        true_name: true_name,
                        mob_phone: mob_phone,
                        tel_phone: tel_phone,
                        city_id: city_id,
                        area_id: area_id,
                        address: address,
                        area_info: area_info,
                        address_id: address_id
                    },
                    dataType: 'json',
                    success: function (result) {
                        if (result) {
                            location.href = ApiUrl + "/index.php?act=member_address&op=address_list";
                        }
                    }
                });
            }
        });
    });
</script>
