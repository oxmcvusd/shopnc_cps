<?php
defined('BYshopJL') or exit('Access Invalid!');

class orderLock
{
    private $dir = BASE_ORDER_LOCK_PATH;

    public function __construct()
    {

        if (!$this->dir) {
            $this->dir = BASE_PATH . '/data/lock/';
        }
    }

    /**
     * lock
     * 加锁处理
     * @param $lockName
     * @return bool
     */
    public function lock($lockName)
    {
        if (file_exists($this->dir . "{$lockName}.lock")) return false;
        $rs = file_put_contents($this->dir . "{$lockName}.lock", $lockName);
        if ($rs) return true;
        return false;
    }

    /**
     * unlock
     * 解锁处理
     * @param $lockName
     * @return bool
     */
    public function unlock($lockName){
        if (file_exists($this->dir ."{$lockName}.lock")){
            @unlink($this->dir ."{$lockName}.lock");
            return true;
        }
        return false;
    }
}