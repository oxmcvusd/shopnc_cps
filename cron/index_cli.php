<?php
/**
 * Description:
 * Author: walkskyer
 * Email:zwj_work@qq.com
 * Date: 2015/4/27
 * Time: 16:08
 */
define('BASE_PATH',str_replace('\\','/',dirname(__FILE__)));
if (!@include(dirname(dirname(__FILE__)).'/global.php')) exit('global.php isn\'t exists!');
if (!@include(BASE_CORE_PATH.'/shopjl.php')) exit('shopjl.php isn\'t exists!');
define('TPL_NAME',TPL_ADMIN_NAME);
define('ADMIN_TEMPLATES_URL',ADMIN_SITE_URL.'/templates/'.TPL_NAME);
define('BASE_TPL_PATH',BASE_PATH.'/templates/'.TPL_NAME);

if (!@include(BASE_PATH.'/control/control.php')) exit('control.php isn\'t exists!');


if (empty($_SERVER['argv'][1]) || empty($_SERVER['argv'][2])) exit('parameter error');

$file_name = strtolower($_SERVER['argv'][1]);

$method = $_SERVER['argv'][2].'Op';
echo "{$file_name}:{$method}\n";
if (!@include(BASE_PATH.'/control/'.$file_name.'.php')) exit($file_name.'.php isn\'t exists!');
$class_name = $file_name.'Control';
$cron = new $class_name();

if (method_exists($cron,$method)){
    @$cron->$method();
}else{
    exit('method '.$method.' isn\'t exists');
}
?>